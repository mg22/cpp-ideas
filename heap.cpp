#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

class Heap {
public:
  Heap() : data_(1, 0) {} 

  Heap(vector<int>&& data) : data_(move(data)) {
    data_.insert(data_.begin(), 0);
    for (int i = data_.size()-1; i >= 1; --i) {
      BubbleDown(i);
    }
  }

  void Push(int val) {
    data_.push_back(val);
    BubbleUp(data_.size()-1);
  }

  void Pop() {
    data_[1] = data_.back();
    data_.pop_back();
    BubbleDown(1);
  }

  int Top() const {
    return data_[1];
  }

  int Size() const {
    return data_.size()-1;
  }

  bool Empty() const {
    return Size() == 0;
  }

private:
  void BubbleDown(int i) {
    for (;;) {
      int left = i<<1;
      int right = (i<<1) | 0x1;
      if (right < data_.size()) {
        if (data_[i] <= data_[left] && data_[i] <= data_[right]) {
          // we are done
          break;
        }
        int next = (data_[left] <= data_[right]) ? left : right;
        swap(data_[i], data_[next]);
        i = next;
      } else {
        if (left < data_.size()) {
          if (data_[i] <= data_[left]) {
            // we are done
            break;
          } else {
            swap(data_[left], data_[i]);
            i = left;
          }
        } else {
          // we are leaf
          break;
        }
      }
    }
  }

  void BubbleUp(int i) {
    for (;;) {
      int parent = i>>1;
      if (parent < 1) {
        break;
      }
      if (data_[parent] <= data_[i]) {
        break;
      }
      swap(data_[i], data_[parent]);
      i = parent;
    }
  }

  vector<int> data_;
};



TEST_CASE("Heap: Basic tests", "[Heap]") {
  vector<int> t = {6, 2, 1, 4, 5, 7, 9, 1, 2};
  vector<int> ex = t;
  sort(ex.begin(), ex.end());
  Heap h;
  for (int x : t) {
    h.Push(x);
  }
  vector<int> o;
  while (!h.Empty()) {
    o.push_back(h.Top());
    h.Pop();
  }
  REQUIRE( ex == o );
}

TEST_CASE("Heap: Random test", "[Heap]")
{
  constexpr int TEST_CASES = 1000;
  constexpr int TEST_CASE_SIZE = 1000;
  constexpr int MAX_ELEMENT = 1000;

  default_random_engine re(random_device{}());
  uniform_int_distribution<int> dist(0, MAX_ELEMENT);

  for (int t = 0; t < TEST_CASES; ++t) {
    vector<int> input(TEST_CASE_SIZE, 0);
    generate(input.begin(), input.end(), [&]() { return dist(re); });

    Heap h;
    for (int x : input) {
      h.Push(x);
    }
    vector<int> sorted;
    while (!h.Empty()) {
      sorted.push_back(h.Top());
      h.Pop();
    }

    vector<int> expected = input;
    sort(expected.begin(), expected.end());

    REQUIRE( sorted == expected);
  }
}

TEST_CASE("Heap: Heapify", "[Heap]") {
  constexpr int TEST_CASES = 1000;
  constexpr int TEST_CASE_SIZE = 1000;
  constexpr int MAX_ELEMENT = 1000;

  default_random_engine re(random_device{}());
  uniform_int_distribution<int> dist(0, MAX_ELEMENT);

  for (int t = 0; t < TEST_CASES; ++t) {
    vector<int> input(TEST_CASE_SIZE, 0);
    generate(input.begin(), input.end(), [&]() { return dist(re); });

    Heap h(vector<int>(input.begin(), input.end()));

    vector<int> sorted;
    while (!h.Empty()) {
      sorted.push_back(h.Top());
      h.Pop();
    }

    vector<int> expected = input;
    sort(expected.begin(), expected.end());

    REQUIRE( sorted == expected );
  }
}
