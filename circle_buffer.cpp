#include <catch2/catch.hpp>
#include <cassert>

// T must be POD
template<typename T>
class PODCircleBuffer
{
public:
    PODCircleBuffer(size_t initila_capacity = 16) :
        _size(0), _capacity(initila_capacity),
        _start(0), _end(_capacity-1),
        _data(new T[_capacity])
    {}

    ~PODCircleBuffer() {
        delete[] _data;
    }

    void PushBack(const T& element) {
        EnsureCapacity();
        _end = (_end+1) % _capacity;
        ++_size;
        _data[_end] = element;
    }

    void PopFront() {
        assert(!Empty());
        _start = (_start + 1) % _capacity;
        --_size;
    }

    T& Front() {
        assert(!Empty());
        return _data[_start];
    }
    T& Back() {
        assert(!Empty());
        return _data[_end];
    }

    size_t Size() const {
        return _size;
    }
    bool Empty() const {
        return Size() == 0;
    }

private:
    void EnsureCapacity() {
        if (_size < _capacity) {
            return;
        }
        size_t new_capacity = _capacity * 2;
        T* new_data = new T[new_capacity];
        size_t new_end = 0;
        for (size_t i = _start; i != _end; i = (i + 1) % _capacity) {
            new_data[new_end] = _data[i];
            ++new_end;
        }
        new_data[new_end] = _data[_end];
        _start = 0;
        _end = new_end;
        _capacity = new_capacity;
        delete[] _data;
        _data = new_data;
    }

    size_t _size;
    size_t _capacity;
    size_t _start;
    size_t _end;
    T* _data;
};


// tests

TEST_CASE("Push and Pop", "[PODCircleBuffer]") {
    PODCircleBuffer<int> c;

    for (int i = 0; i < 5; ++i) {
        c.PushBack(i);
    }

    REQUIRE(c.Size() == 5);
    REQUIRE(!c.Empty());

    for (int i = 0; i < 3; ++i) {
        REQUIRE(c.Front() == i);
        c.PopFront();
    }

    REQUIRE(c.Size() == 2);

    for (int i = 3; i < 5; ++i) {
        REQUIRE(c.Front() == i);
        c.PopFront();
    }

    REQUIRE(c.Size() == 0);
    REQUIRE(c.Empty() == true);
}

TEST_CASE("Sliding window", "[PODCircleBuffer]") {
    const int window = 50;
    PODCircleBuffer<int> c(window/2);
    for (int i = 0; i < window; ++i) {
        c.PushBack(i);
    }
    REQUIRE(c.Size() == window);

    int slides = 200;
    for (int s = 0; s < slides; ++s) {
        for (int i = 0; i < window-1; ++i) {
            REQUIRE(c.Front() == i);
            c.PopFront();
        }
        REQUIRE(c.Size() == 1);
        for (int i = 0; i < window; ++i) {
            c.PushBack(i);
        }
        REQUIRE(c.Size() == (window+1));
        c.PopFront();
        REQUIRE(c.Size() == window);
    }

    REQUIRE(c.Size() == window);
}

TEST_CASE("Fill and dump", "[PODCircleBuffer]") {
    PODCircleBuffer<int> c;

    for (int i = 0; i < 500; ++i) {
        c.PushBack(i);
    }
    REQUIRE(c.Size() == 500);
    for (int i = 0; i < 500; ++i) {
        c.PopFront();
    }
    REQUIRE(c.Empty());
}