#include <ssv_reader.h>

#include <sstream>
#include <variant>
#include <iostream>

int main()
{
    std::stringstream one_line(
      "first line data1\n"
      "second line data2\n"
      "third line data3\n");

    auto result = ssv_reader::read(one_line);

    std::visit(ssv_reader::unpack{
                 [](ssv_reader::ssv_lines &lines) {
                     std::cout << "--parsed-begin--\n";
                     for (auto &line : lines) {
                         for (auto &column : line) {
                             std::cout << column << " ";
                         }
                         std::cout << "\n";
                     }
                     std::cout << "--parsed-end--\n";
                 },
                 [](ssv_reader::errors::error &error) {
                     std::cout << "error " << error.reason << "\n";
                 } },
      result);
}