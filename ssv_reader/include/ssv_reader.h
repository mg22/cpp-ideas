#include <iostream>
#include <istream>
#include <sstream>
#include <vector>
#include <string>
#include <array>
#include <variant>

namespace ssv_reader {

namespace detail {

std::vector<std::string> extract_columns(const std::string &line)
{
    std::stringstream line_stream(line);
    int columns = 0;
    std::string column_buffer;
    std::vector<std::string> result;
    while (line_stream >> column_buffer) {
        result.push_back(column_buffer);
    }
    return result;
}

};// namespace detail

namespace errors {

// todo: better organization than this

enum class codes {
    OK = 0,
    UNSPECIFIED,
    UNEVEN_COLUMNS,
};

struct error
{
    codes code;
    const char *reason;
};

// group of predefined errors
static error OK = { codes::OK, "ok" };
static error UNSPECIFIED = { codes::UNSPECIFIED, "dunno" };
static error UNEVEN_COLUMNS = { codes::UNEVEN_COLUMNS, "uneven columns" };

static std::array<error, 3>
  ALL_ERRORS = {
      OK,
      UNSPECIFIED,
      UNEVEN_COLUMNS,
  };

};// namespace errors


using ssv_lines = std::vector<std::vector<std::string>>;

// helper to unpack variant result
template<class... Ts>
struct unpack : Ts...
{
    using Ts::operator()...;
};
template<class... Ts>
unpack(Ts...) -> unpack<Ts...>;

std::variant<ssv_lines, errors::error> read(std::istream &input)
{
    ssv_lines result;

    int num_columns = -1;
    for (std::string line; std::getline(input, line);) {
        auto extracted_line = detail::extract_columns(line);

        // check for column consistency
        if (num_columns == -1) {
            num_columns = extracted_line.size();
        } else {
            if (num_columns != extracted_line.size()) {
                return errors::UNEVEN_COLUMNS;
            }
        }

        result.push_back(std::move(extracted_line));
    }

    return result;
}

};// namespace ssv_reader