#include "gtest/gtest.h"

#include "ssv_reader.h"

TEST(BasicTests, MultilineReadSuccessfull)
{
    std::string line =
      "first line of data \n"
      "second     line  of data\n"
      "111 22   333 444    \n"
      "   5    6 7   8 \n";

    std::vector<std::vector<std::string>> correct_lines = {
        { "first", "line", "of", "data" },
        { "second", "line", "of", "data" },
        { "111", "22", "333", "444" },
        { "5", "6", "7", "8" }
    };
    std::stringstream input(line);

    auto result = ssv_reader::read(input);

    ASSERT_TRUE(std::holds_alternative<ssv_reader::ssv_lines>(result));

    auto &result_lines = std::get<ssv_reader::ssv_lines>(result);
    ASSERT_EQ(result_lines.size(), correct_lines.size());
    for (size_t i = 0; i < result_lines.size(); ++i) {
        auto &result_line = result_lines[i];
        auto &correct_line = correct_lines[i];
        ASSERT_EQ(result_line.size(), correct_line.size());
        for (size_t j = 0; j < result_line.size(); ++j) {
            ASSERT_EQ(result_line[j], correct_line[j]);
        }
    }
}

TEST(BasicTests, EmptyLineReadSuccessfull)
{
    std::string line = "";
    std::stringstream input(line);

    auto result = ssv_reader::read(input);

    ASSERT_TRUE(std::holds_alternative<ssv_reader::ssv_lines>(result));

    auto &result_lines = std::get<ssv_reader::ssv_lines>(result);
    ASSERT_EQ(result_lines.size(), 0);
}

TEST(BasicTests, UnevenColumnsReadFails)
{
    std::string line =
      "first line of data \n"
      "second     line  of more data\n"
      "111 22   333 444    \n"
      "   5    6 7   8 \n";

    std::stringstream input(line);

    auto result = ssv_reader::read(input);

    ASSERT_TRUE(std::holds_alternative<ssv_reader::errors::error>(result));

    auto &error = std::get<ssv_reader::errors::error>(result);
    ASSERT_EQ(error.code, ssv_reader::errors::codes::UNEVEN_COLUMNS);
}
