#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

int MinimumMessinessHelper(int pos, const vector<string>& words, int width, vector<int>& memo) {
  if (pos == words.size()) {
    return 0;
  }

  if (memo[pos] != -1) {
    return memo[pos];
  }

  int min_messiness = numeric_limits<int>::max();
  int current_width = width;
  for (int i = pos; i < words.size(); ++i) {
    // add next word
    if (i != pos) {
      // space
      --current_width;
    }
    current_width -= words[i].size();
    if (current_width < 0) {
      break;
    }
    min_messiness = min(
      min_messiness,
      current_width*current_width + MinimumMessinessHelper(i+1, words, width, memo)
    );
  }
  return memo[pos] = min_messiness;
}

int MinimumMessiness(const vector<string>& words, int width) {
  vector<int> memo(words.size(), -1);
  return MinimumMessinessHelper(0, words, width, memo);
}

TEST_CASE("EPI_16_11: Basic tests", "[EPI_16_11]") {
  REQUIRE( MinimumMessiness({"a", "b", "c", "d"}, 5) == 8 );
  REQUIRE( MinimumMessiness({"aaa", "bbb", "c", "d", "ee"}, 11) == 41 );
  REQUIRE( MinimumMessiness({"aaa", "bbb", "c", "d", "ee", "ff"}, 11) == 20 );
  REQUIRE( MinimumMessiness({"aaa", "bbb", "c", "d", "ee", "ff", "ggggggg"}, 11) == 36);
  REQUIRE( MinimumMessiness({"a", "b", "c", "d"}, 1) == 0 );
  REQUIRE( MinimumMessiness({"a", "b", "c", "d"}, 2) == 4 );
}