#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;


vector<int> KMPPreprocess(const string& pattern) {
  vector<int> overlap(pattern.size()+1);
  overlap[0] = -1;
  for (size_t i = 1; i < overlap.size(); ++i) {
    overlap[i] = overlap[i-1]+1;
    while (overlap[i] > 0 && pattern[overlap[i]-1] != pattern[i-1]) {
      overlap[i] = overlap[overlap[i]-1]+1;
    }
  }
  return overlap;
}


vector<size_t> KMPMatch(const string& text, const string& pattern) {
  auto overlap = KMPPreprocess(pattern);

  // for (char c : pattern) {
  //   cout << c << " ";
  // }
  // cout << "\n";
  // for (int i = 1; i < overlap.size(); ++i) {
  //   cout << overlap[i] << " ";
  // }
  // cout << "\n";

  vector<size_t> matches;
  
  size_t j = 0;
  for (size_t i = 0; i < text.size(); ++i) {
    for (;;) {
      if (text[i] == pattern[j]) {
        ++j;
        if (j == pattern.size()) {

          // cout << "match at " << (i-j+1) << "\n";
          matches.push_back(i-j+1);

          j = overlap[j];
        }
        break;
      } else if (j == 0) {
        break; // no match even at the start of pattern
      } else {
        j = overlap[j];
      }
    }
  }

  return matches;
}

TEST_CASE("KMP: Basic tests", "[KMP]") {
  string t = "xyxxyxyxyyxyxyxyyxyxyxxy";
  string p =             "xyxyyxyxyxx";
  //                      ^
  //                      12
  
  REQUIRE( KMPMatch(t, p) == vector<size_t>{ 12 } );
  REQUIRE( KMPMatch("abcd", "c") == vector<size_t>{ 2 } );
  REQUIRE( KMPMatch("abcd", "x") == vector<size_t>{ } );
  REQUIRE( KMPMatch("abcd", "a") == vector<size_t>{ 0 } );
  REQUIRE( KMPMatch("abcd", "d") == vector<size_t>{ 3 } );
  REQUIRE( KMPMatch("adbaccadaacdacad", "ad") == vector<size_t>{ 0, 6, 14 } );
}