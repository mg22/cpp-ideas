#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

template<typename K, typename V>
class HashTable {
public:
  HashTable() : size_(0), capacity_(MIN_CAPACITY), data_(capacity_) {}

  bool Insert(K key, V value) {
    if (size_+ 1 >= capacity_ * LOAD_FACTOR) {
      Enlarge();
    }
    size_t h = Hash(key);
    for (auto& n : data_[h]) {
      if (n.first == key) {
        n.second = value;
        return false;
      }
    }
    data_[h].push_back({key, value});
    ++size_;
    return true;
  }

  optional<V> Get(K key) {
    size_t h = Hash(key);
    for (auto& n : data_[h]) {
      if (n.first == key) {
        return n.second;
      }
    }
    return {};
  } 

  bool Remove(K key) {
    size_t h = Hash(key);
    if (data_[h].empty()) {
      return false;
    }
    for (auto it = data_[h].begin(); it != data_[h].end(); ++it) {
      if (it->first == key) {
        data_[h].erase(it);
        --size_;
        return true;
      }
    }
    return false;
  }

  size_t Size() const {
    return size_;
  }

  bool Empty() const {
    return Size() == 0;
  }

private:
  size_t Hash(K key) {
    return hash<K>{}(key) % capacity_;
  }
  void Enlarge() {
    capacity_ *= 2;
    vector<list<pair<K,V>>> new_data(capacity_);
    for (auto& l : data_) {
      for (auto& [k, v] : l) {
        size_t h = Hash(k);
        new_data[h].push_back({k, v});
      }
    }
    swap(new_data, data_);
  }

  static const size_t MIN_CAPACITY = 32;
  static constexpr double LOAD_FACTOR = 0.75;

  size_t size_;
  size_t capacity_;
  vector<list<pair<K,V>>> data_;
};


TEST_CASE("Hash table chaining: Basic tests", "[Hash table chaining]") {
  HashTable<string,int> t;
  REQUIRE( t.Insert("abcd", 1) == true );
  REQUIRE( t.Insert("aaaa", 47) == true );
  REQUIRE( t.Insert("a", 1) == true );
  REQUIRE( t.Insert("b", 2) == true );
  REQUIRE( t.Insert("c", 3) == true );
  REQUIRE( t.Insert("d", 4) == true );

  SECTION( "data are actually inside") {
    REQUIRE( t.Get("abcd") );
    REQUIRE( t.Get("aaaa") );
    REQUIRE( t.Get("a") );
    REQUIRE( t.Get("b") );
    REQUIRE( t.Get("c") );
    REQUIRE( t.Get("d") );
  }

  SECTION( "remove works" ) {
    REQUIRE( t.Remove("xxxx") == false );
    REQUIRE( t.Remove("abcd") == true );
    REQUIRE_FALSE( t.Get("abcd") );
  }

  SECTION( "update of element" ) {
    REQUIRE( t.Get("a") == 1 );
    t.Insert("a", 4);
    REQUIRE( t.Get("a") == 4 );
  }
  
  SECTION( "size works" ) {
    REQUIRE( t.Size() == 6 );
    t.Insert("e", 22);
    REQUIRE( t.Size() == 7 );
    t.Remove("aaaa");
    REQUIRE( t.Size() == 6 );
  }

  SECTION( "insert same works" ) {
    REQUIRE( t.Insert("aaaa", 47) == false );
    REQUIRE( t.Insert("aaaa", 48) == false );
    REQUIRE( t.Insert("d", 48) == false );
  }
}

TEST_CASE("Hash table chaining: Many inserts", "[Hash table chaining]") {
  HashTable<int,int> t;

  default_random_engine re(random_device{}());
  uniform_int_distribution<int> dist(0, 1000);
  vector<pair<int,int>> data;
  for (int i = 0; i < 10000; ++i) {
    data.push_back({dist(re), dist(re)});
    t.Insert(data.back().first, data.back().second);
  }

  for (int i = 0; i < 10000; ++i) {
    REQUIRE( t.Get(data[i].first) );
  }

  for (int i = 0; i < 10000; ++i) {
    REQUIRE_FALSE( t.Insert(data[i].first, data[i].second) );
  }

  for (int i = 0; i < 10000/2; ++i) {
    t.Remove(data[i].first);
  }
  for (int i = 0; i < 10000/2; ++i) {
    REQUIRE_FALSE( t.Remove(data[i].first) );
    REQUIRE_FALSE( t.Get(data[i].first) );
  }
}
