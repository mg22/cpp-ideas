#include <iostream>
using namespace std;

template<typename QueryRow>
struct Query {};

struct QueryParams {};

template<typename Query, typename QueryParams>
class Queries {
    protected:
        template<typename ... FactoryFnArgs>
        Query* CreateAndRegisterQuery(QueryParams params, Query*(*factory_fn)(QueryParams, FactoryFnArgs...), FactoryFnArgs ... args);
};

template<typename Query, typename QueryParams>
template<typename ... FactoryFnArgs>
Query* Queries<Query, QueryParams>::CreateAndRegisterQuery(QueryParams params, Query*(*factory_fn)(QueryParams, FactoryFnArgs...), FactoryFnArgs ... args)
{
    return factory_fn(params, args...);
}

struct LogQueryRow {};
struct LogQuery : public Query<LogQueryRow> {};
struct LogQueryParams : public QueryParams {};

class LogQueries : public Queries<LogQuery, LogQueryParams> {
    public:
        LogQuery* Do(LogQueryParams params);

    protected:
        static LogQuery* LogQueryFactory(LogQueryParams params, int x);

    private:
        //int x = 6;
};

LogQuery* LogQueries::Do(LogQueryParams params)
{
    //auto fn = [this] (LogQueryParams params, int x) -> LogQuery* { return LogQueryFactory(params, x); };
    return CreateAndRegisterQuery<int>(params, LogQueryFactory, 4);
}

LogQuery* LogQueries::LogQueryFactory(LogQueryParams /*params*/, int x)
{
    cout << x << endl;
    return new LogQuery;
}

int main() {
    LogQueries q;
    LogQueryParams p;
    auto x = q.Do(p);
    delete x;
    return 0;
}
