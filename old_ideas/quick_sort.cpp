#include <cassert>

#include <chrono>
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

mt19937 g_mt((random_device())());

template<typename I>
void print_vec(I begin, I end) {
    while (begin != end) {
        cout << *begin << " ";
        ++begin;
    }
    cout << "\n";
}

template<typename I>
void quick_sort(I begin, I end) {
    int distance = end - begin;
    if (distance <= 1) {
        return;
    } 

    I pivot = begin + uniform_int_distribution<int>(0, distance-1)(g_mt);
    swap(*pivot, *(end-1));
    pivot = end-1;

    typename I::value_type pivot_val = *pivot;
    I target = begin;
    for (I i = begin; i != end; ++i) {
        if (*i <= pivot_val) {
            swap(*i, *target);
            ++target;
        }
    }

    quick_sort(begin, target-1);
    quick_sort(target, end);
}

template<typename I>
void quick_sort2(I begin, I end)
{
    size_t distance = end - begin;
    if (distance <= 1) {
        return;
    }

    typename I::value_type pivot = *(begin + uniform_int_distribution<int>(0, distance-1)(g_mt));
    I less = begin;
    I equal = begin;
    I greater = end;

    while (equal < greater) {
        typename I::value_type current = *equal;
        if (current < pivot) {
            swap(*equal, *less);
            ++less;
            ++equal;
        } else if (current == pivot) {
            ++equal;
        } else {
            // current > pivot
            --greater;
            swap(*equal, *greater);
        }
    }
    // cout << "pivot val: " << pivot << "\n";
    // cout << "whole: \n";
    // print_vec(greater, end);

    // cout << "left: \n";
    // print_vec(begin, less);
    quick_sort2(begin, less);
    // cout << "right: \n";
    // print_vec(greater, end);
    quick_sort2(greater, end);
}

bool vec_equal(const vector<int>& v1, const vector<int> v2, int* inequal = nullptr) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (int i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            if (inequal) {
                *inequal = i;
            }
            return false;
        }
    }
    return true;
}


constexpr int TEST_CASES = 100;
constexpr int TEST_CASE_SIZE = 100000;
constexpr int MIN_ELEMENT = 100;
constexpr int MAX_ELEMENT = 100000;


template<typename T>
void do_test(T sort_func)
{
    mt19937_64 random_engine((random_device())());
    uniform_int_distribution<int> input_distr(MIN_ELEMENT, MAX_ELEMENT);

    for (int i = 0; i < TEST_CASES; ++i) {
        vector<int> vec(TEST_CASE_SIZE, 0);
        generate(vec.begin(), vec.end(), [&](){ return input_distr(random_engine); });
        vector<int> expected = vec;

        auto t_tested_start = chrono::high_resolution_clock::now();
        sort_func(vec.begin(), vec.end());
        auto t_tested_end = chrono::high_resolution_clock::now();

        auto t_std_start = chrono::high_resolution_clock::now();
        ::std::sort(expected.begin(), expected.end());
        auto t_std_end = chrono::high_resolution_clock::now();
        
        int inequal_index;
        if (!vec_equal(vec, expected, &inequal_index)) {
            cout << "----- sort_func:\n";
            for (int i = 0; i < vec.size(); ++i) {
                if (i == inequal_index) {
                    cout << "~";
                }
                cout << vec[i];
                if (i == inequal_index) {
                    cout << "~";
                }
                cout << " ";
            }
            cout << "\n";
            cout << "----- std::sort:\n";
            for (int i = 0; i < expected.size(); ++i) {
                if (i == inequal_index) {
                    cout << "~";
                }
                cout << expected[i];
                if (i == inequal_index) {
                    cout << "~";
                }
                cout << " ";
            }
            cout << "\n";
        }
        assert(vec_equal(vec, expected) == true);

        chrono::duration<double> elapsed_sort_func = t_tested_end - t_tested_start;
        chrono::duration<double> elapsed_std = t_std_end - t_std_start;
        std::cout << "sort_func: " << elapsed_sort_func.count() << "\n";
        std::cout << "std::sort: " << elapsed_std.count() << "\n";
    }
}

int main() {
    // do_test<void(*)(vector<int>::iterator, vector<int>::iterator)>(quick_sort);
    do_test<decltype(quick_sort<vector<int>::iterator>)>(quick_sort);
    do_test<decltype(quick_sort<vector<int>::iterator>)>(quick_sort2);
}
