#include <cassert>

#include <chrono>
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

void merge_halves(vector<int>& v, int start, int mid, int end) {
    vector<int> merged;
    int s1 = start;
    int s2 = mid;

    while (s1 < mid && s2 < end) {
        if (v[s1] < v[s2]) {
            merged.push_back(v[s1]);
            ++s1;
        } else {
            merged.push_back(v[s2]);
            ++s2;
        }
    }

    while (s1 < mid) {
        merged.push_back(v[s1]);
        ++s1;
    }
    while (s2 < end) {
        merged.push_back(v[s2]);
        ++s2;
    }

    for (int i = 0; i < merged.size(); ++i) {
        v[start + i] = merged[i];
    }
}

// [start, end)
void merge_sort_helper(vector<int>& v, int start, int end) {
    if ((end - start) < 2) {
        return;
    }
    int mid = start + (end-start)/2;
    merge_sort_helper(v, start, mid);
    merge_sort_helper(v, mid, end);
    merge_halves(v, start, mid, end);
}

void merge_sort(vector<int>& v) {
    merge_sort_helper(v, 0, v.size());
}


bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (int i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

int main() {
    
    // auto t1 = chrono::high_resolution_clock::now();
    // auto t2 = chrono::high_resolution_clock::now();
    // chrono::duration<double> elapsed = t2 - t1;
    // std::cout << "duration: " << elapsed.count() << "\n";
    random_device rand_dev;
    mt19937 rand_engine(rand_dev());
    uniform_int_distribution<int> input_distr(0, 10000000);

    for (int i = 0; i < 100; ++i) {
        vector<int> vec(1000000, 0);
        generate(vec.begin(), vec.end(), [&](){ return input_distr(rand_engine); });
        

        vector<int> expected = vec;
        auto t1 = chrono::high_resolution_clock::now();
        sort(expected.begin(), expected.end());
        auto t2 = chrono::high_resolution_clock::now();

        auto t3 = chrono::high_resolution_clock::now();
        merge_sort(vec);
        auto t4 = chrono::high_resolution_clock::now();
        
        assert(vec_equal(vec, expected) == true);

        chrono::duration<double> elapsed_std = t2 - t1;
        chrono::duration<double> elapsed_merge = t4 - t3;
        std::cout << "std::sort: " << elapsed_std.count() << "\n";
        std::cout << "merge_sort: " << elapsed_merge.count() << "\n";
    }
}