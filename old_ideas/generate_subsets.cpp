#include <iostream>
#include <vector>

using namespace std;

template <typename T>
void get_all_subsets_internal(const vector<T>& elements, size_t processed,
                              vector<T>& current_set,
                              vector<vector<T>>& results) {
    if (processed == elements.size()) {
        results.push_back(current_set);
        return;
    }
    // take current element
    current_set.push_back(elements[processed]);
    get_all_subsets_internal(elements, processed + 1, current_set, results);
    current_set.pop_back();
    get_all_subsets_internal(elements, processed + 1, current_set, results);
}

template <typename T>
vector<vector<T>> get_all_subsets(const vector<T>& elements) {
    vector<vector<T>> result;
    vector<T> temporary_storage;
    get_all_subsets_internal(elements, 0, temporary_storage, result);
    return result;
}

template <typename T>
void print_2d_vector(const vector<vector<T>>& to_print) {
    cout << "printing vector -----" << endl;
    for (const auto& val : to_print) {
        for (const auto& i : val) {
            cout << i << " ";
        }
        cout << endl;
    }
}

int main() {
    vector<int> v = {1, 2, 4, 5, 6, 10};
    auto res = get_all_subsets(v);
    print_2d_vector(res);
    return 0;
}
