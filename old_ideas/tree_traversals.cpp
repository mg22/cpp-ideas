#include <iostream>
#include <stack>

using namespace std;

template <typename Data> struct Node {
  Data data;
  Node *left, *right;

  Node(Node *left, Node *right, Data data)
      : data(data), left(left), right(right) {}
  Node(Data data) : data(data), left(nullptr), right(nullptr) {}
};

typedef Node<int> IntNode;

void postorder_stack(IntNode *root) {
  struct NodeWithDirection {
    enum { UP, DOWN } direction;
    IntNode *node;
  };

  stack<NodeWithDirection> node_stack;
  node_stack.push({NodeWithDirection::DOWN, root});
  while (!node_stack.empty()) {
    auto top = node_stack.top();
    node_stack.pop();
    if (top.node == nullptr) {
      continue;
    }
    if (top.direction == NodeWithDirection::DOWN) {
      node_stack.push({NodeWithDirection::UP, top.node});
      node_stack.push({NodeWithDirection::DOWN, top.node->right});
      node_stack.push({NodeWithDirection::DOWN, top.node->left});
    } else {
      cout << top.node->data << endl;
    }
  }
}

int main() {
  IntNode *tree = new IntNode(
      new IntNode(
          new IntNode(new IntNode(28), new IntNode(0), 271),
          new IntNode(nullptr, new IntNode(new IntNode(17), nullptr, 3), 561),
          6),
      new IntNode(
          new IntNode(nullptr,
                      new IntNode(new IntNode(nullptr, new IntNode(641), 401),
                                  new IntNode(257), 1),
                      2),
          new IntNode(nullptr, new IntNode(28), 271), 6),
      314);

  postorder_stack(tree);
}
