#include <cassert>
#include <vector>
#include <random>
#include <algorithm>
#include <iostream>

using namespace std;



template<typename It, typename T>
It lower_bound1(It start, It end, const T& val)
{
    It result = end;
    --end;
    ssize_t diff = end-start;

    while (diff >= 0) {
        It mid = start + diff/2;
        if (*mid >= val) {
            if (*mid == val) {
                result = mid;
            }
            end = mid-1;
        } else {
            // *mid < val
            start = mid+1;
        }
        diff = end-start;
    }
    return result;
}











template<typename It, typename T>
It lower_bound2(It start, It end, const T& val)
{
    It lo = start;
    It hi = end;
    --hi;
    size_t dist = hi - lo;
    if (val <= *lo) {
        return lo;
    }

    while (dist > 1) {
        It mid = lo + dist/2;
        if (val <= *hi) {
            hi = mid;
        } else {
            lo = mid;
        }
        dist = hi - lo;
    }
    return val == *hi ? hi : end;
    
}







template<typename It, typename T>
It lower_bound3(It start, It end, const T& val)
{
    It lo = start;
    It hi = end - 1;
    ssize_t dist = hi - lo;
    It result = end;
    while (dist >= 0) {
        It mid = lo + dist/2;
        if (val <= *mid) {
            if (val == *mid) {
                result = mid;
            }
            hi = mid-1;
        } else {
            lo = mid+1;
        }
        dist = hi - lo;
    }
    return result;
}




constexpr int TEST_CASES = 200;
constexpr int TEST_CASE_SIZE = 100000;
constexpr int TEST_CASE_SEARCHES = 1000;
constexpr int MIN_ELEMENT = 100;
constexpr int MAX_ELEMENT = 1000;

template<typename T>
void do_tests(T lb)
{
    random_device rd;
    mt19937 re(rd());
    uniform_int_distribution<int> dist(MIN_ELEMENT, MAX_ELEMENT);

    for (int i = 0; i < TEST_CASES; ++i) {
        vector<int> vec(TEST_CASE_SIZE, 0);
        generate(vec.begin(), vec.end(), [&](){ return dist(re); });
        sort(vec.begin(), vec.end());
        
        //vec.erase(unique(vec.begin(), vec.end()), vec.end());
    
        // for (int s = 0; s < TEST_CASE_SEARCHES; ++s) {
        //     int index = uniform_int_distribution<int>(0, vec.size()-1)(re);
        //     int value = vec[index];
        //     auto res = lb(vec.begin(), vec.end(), value);
        //     auto expected = lower_bound(vec.begin(), vec.end(), value);
        //     assert(res == expected);
        // }

        for (int s = 0; s < TEST_CASE_SEARCHES; ++s) {
            int value = MAX_ELEMENT+1+s;
            auto res = lb(vec.begin(), vec.end(), value);
            auto expected = lower_bound(vec.begin(), vec.end(), value);
            assert(res == expected);
        }

        // for (int s = 0; s < TEST_CASE_SEARCHES; ++s) {
        //     int element = MAX_ELEMENT+s+1;
        //     int result = bs(vec, element);
        //     assert(result == -1);
        // }
        // for (int s = 0; s < MIN_ELEMENT; ++s) {
        //     int result = bs(vec, s);
        //     assert(result == -1);
        // }
    }
}

// typename lower_bound1<vector<int>::iterator>, int> lower_bound11;

int main() {
    // do_tests(lower_bound1<vector<int>::iterator,int>);
    do_tests(lower_bound2<vector<int>::iterator,int>);
    // do_tests(lower_bound3<vector<int>::iterator,int>);
}

