#include <cassert>

#include <bits/stdc++.h>
using namespace std;

template<typename T>
class Stack {
    public:
        Stack() {}

        void Push(T t) {
            stack_.push_back(t);
        }

        void Pop() {
            stack_.pop_back();
        }

        T& Top() {
            return stack_.back();
        }

        size_t Size() { 
            return stack_.size();
        }

        bool Empty() {
            return Size() == 0;
        }

    private:
        vector<T> stack_;
};


int main() {
    Stack<int> s;
    s.Push(1);
    s.Push(2);
    s.Push(3);
    s.Push(4);
    s.Push(5);

    assert(s.Size() == 5);
    vector<int> expected = {5, 4, 3, 2, 1};
    int c = 0;

    while (!s.Empty()) {
        assert(s.Top() == expected[c]);
        ++c;
        s.Pop();
    }
    assert(s.Size() == 0);
}