#include <cassert>
#include <bits/stdc++.h>
using namespace std;

template<typename T>
class LinkedList {
    private:
        struct Node;
        struct ValNode;

    public:
        class Iterator {
            public:
                T& operator *() {
                    // assert node_->next != nullptr
                    return static_cast<ValNode*>(node_)->val;
                }

                operator bool() const {
                    return node_->next != nullptr;
                }

                bool operator=(const Iterator& o) const {
                    return node_ == o.node_;
                }

                void operator++() {
                    if (node_->next) {
                        node_ = node_->next;
                    }
                }

                void operator--() {
                    if (node_->prev) {
                        node_ = node_->prev;
                    }
                }

            private:
                Iterator(Node* node) : node_(node) {}

                Node* node_;

                friend LinkedList;
        };

        LinkedList()
        : endNode_{nullptr, nullptr}, size_(0), front_(&endNode_)
        {}
        ~LinkedList() {
            Node* current = front_;
            while (current != &endNode_) {
                Node* next = current->next;
                delete static_cast<ValNode*>(current);
                current = next;
            }
        }

        void PushFront(const T& val) {
            InsertInternal(front_, val);
        }

        void PushFront(T&& val) {
            InsertInternal(front_, std::move(val));
        }

        void PushBack(const T& val) {
            InsertInternal(&endNode_, val);
        }

        void PushBack(T&& val) {
            InsertInternal(&endNode_, std::move(val));
        }

        void PopFront() {
            DeleteInternal(front_);
        }

        void PopBack() {
            DeleteInternal(endNode_.prev);
        }

        T& Front() {
            return static_cast<ValNode*>(front_)->val;
        }

        T& Back() {
            return static_cast<ValNode*>(endNode_.prev)->val;
        }

        Iterator Begin() {
            return Iterator(front_);
        }

        Iterator End() {
            return Iterator(&endNode_);
        }

        void Insert(Iterator it, const T& val) {
            InsertInternal(it.node_, val);
        }
        
        void Insert(Iterator it, T&& val) {
            InsertInternal(it.node_, val);
        }

        void Delete(Iterator it) {
            DeleteInternal(it.node_);
        }

        size_t Size() const {
            return size_;
        }

        bool Empty() const {
            return Size() == 0;
        }


    private:
        // Node* is ValNode* iff next != nullptr
        struct Node {
            Node* next;
            Node* prev;
        };
        struct ValNode : public Node {
            T val;
        };

        void InsertInternal(Node* where, const T& val) {
            ValNode* newNode = new ValNode;
            newNode->val = val;
            InsertInternalNode(where, newNode);
        }
        void InsertInternal(Node* where, T&& val) {
            ValNode* newNode = new ValNode;
            newNode->val = std::move(val);
            InsertInternalNode(where, newNode);
        }

        void InsertInternalNode(Node* where, ValNode* newNode) {
            newNode->next = where;
            newNode->prev = where->prev;
            if (where->prev) {
                where->prev->next = newNode;
            }
            where->prev = newNode;

            if (where == front_) {
                front_ = newNode;
            }
            ++size_;
        }

        void DeleteInternal(Node* where) {
            // where is not endNode_
            if (where == &endNode_) {
                return;
            }
            Node* nextNode = where->next;
            nextNode->prev = nextNode->prev->prev;
            if (nextNode->prev) {
                nextNode->prev->next = nextNode;
            }

            if (where == front_) {
                front_ = where->next;
            }

            delete static_cast<ValNode*>(where);
            --size_;
        }

        Node endNode_;
        size_t size_;
        Node* front_;
};

bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

int main()
{
    mt19937_64 twister((random_device())());
    uniform_int_distribution<int> distr(100, 10000);

      {
        vector<int> vec(100000, 0);
        generate(vec.begin(), vec.end(), [&](){ return distr(twister); });
        LinkedList<int> l;
        for (int x : vec) {
            l.PushBack(x);
        }
        vector<int> out;
        while (!l.Empty()) {
            out.push_back(l.Front());
            l.PopFront();
        }
        assert(vec_equal(vec, out));
        assert(l.Empty());
    }

    {
        vector<int> vec(100000, 0);
        generate(vec.begin(), vec.end(), [&](){ return distr(twister); });
        LinkedList<int> l;
        for (int x : vec) {
            l.PushFront(x);
        }
        vector<int> out;
        while (!l.Empty()) {
            out.push_back(l.Back());
            l.PopBack();
        }
        assert(vec_equal(vec, out));
        assert(l.Empty());
    }

    {
        LinkedList<int> l;
        l.PushBack(1);
        l.PushBack(2);
        l.PushBack(3);
        l.PushBack(4);
        l.PushBack(5);

        assert(l.Size() == 5);

        vector<int> out;
        for (auto it = l.Begin(); it != l.End(); ++it) {
            out.push_back(*it);
        }
        vector<int> expected = {1, 2, 3, 4, 5};
        assert(vec_equal(out, expected));
    }
    
    {
        LinkedList<int> l;
        l.PushBack(1);
        l.PushBack(2);
        l.PushBack(3);
        l.PushBack(4);
        l.PushBack(5);

        assert(l.Size() == 5);

        auto it = l.Begin();
        ++it;
        ++it;
        assert(*it == 3);
        l.Insert(it, 7);

        vector<int> out;
        for (auto it = l.Begin(); it != l.End(); ++it) {
            out.push_back(*it);
        }
        vector<int> expected = {1, 2, 7, 3, 4, 5};
        assert(vec_equal(out, expected));
    }

    {
        LinkedList<int> l;
        l.PushBack(1);
        l.PushBack(2);
        l.PushBack(3);
        l.PushBack(4);
        l.PushBack(5);

        assert(l.Size() == 5);

        auto it = l.Begin();
        ++it;
        ++it;
        ++it;
        assert(*it == 4);
        l.Delete(it);

        vector<int> out;
        for (auto it = l.Begin(); it != l.End(); ++it) {
            out.push_back(*it);
        }
        vector<int> expected = {1, 2, 3, 5};
        assert(vec_equal(out, expected));
    }
}