#ifndef __RING_BUFFER_H_
#define __RING_BUFFER_H_

#include <cstdint>
#include <utility>

template<typename T>
class RingBuffer {
    public:
        typedef T value_type;
        typedef value_type* pointer;
        typedef const value_type* const_pointer;
        typedef value_type& reference;
        typedef const value_type& const_reference;
        typedef std::size_t size_type;

        explicit RingBuffer(size_type capacity = 100);
        ~RingBuffer();

        reference front() { return array[head]; }
        reference back() { return array[tail]; }
        const_reference front() const { return array[head]; }
        const_reference back() const { return array[tail]; }

        void clear() { head = tail = content_size = 0; }
        bool full() { return content_size == array_size; }

        void push_back(const value_type& val);
        template<typename... Args>
        void emplace_back(Args&&... args);
        void pop_front();

        // non-copyable
        RingBuffer(const RingBuffer&) = delete;
        RingBuffer& operator= (const RingBuffer&) = delete;
    private:
        void increment_head();
        void increment_tail();

        pointer array;
        size_type array_size;
        size_type content_size;
        size_type head;
        size_type tail;
};

template<typename T>
RingBuffer<T>::RingBuffer(size_type capacity)
    : array(new value_type[capacity]),
      array_size(capacity),
      content_size(0),
      head(0),
      tail(capacity-1)
{}

template<typename T>
RingBuffer<T>::~RingBuffer()
{
    delete[] array;
}

template<typename T>
void RingBuffer<T>::push_back(const value_type& val)
{
    increment_tail();
    if (content_size == array_size) {
        increment_head();
    }
    array[tail] = val;
}

template<typename T>
template<typename... Args>
void RingBuffer<T>::emplace_back(Args&&... args)
{
    increment_tail();
    if (content_size == array_size) {
        increment_head();
    }
    array[tail] = T(std::forward<Args>(args)...);
}

template<typename T>
void RingBuffer<T>::pop_front()
{
    increment_head();
}

template<typename T>
void RingBuffer<T>::increment_head()
{
    ++head;
    if (head == array_size) head= 0;
    --content_size;
}

template<typename T>
void RingBuffer<T>::increment_tail()
{
    ++tail;
    if (tail == array_size) tail = 0;
    ++content_size;
}

#endif //__RING_BUFFER_H_
