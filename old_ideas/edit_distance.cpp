#include <iostream>
#include <limits>
#include <vector>

using namespace std;

int edit_distance_helper(const string &s1, int s1_pos, const string &s2,
                         int s2_pos, vector<vector<int>> &memo) {
  if (s1_pos < 0) {
    return s2_pos + 1;
  }
  if (s2_pos < 0) {
    return s1_pos + 1;
  }

  if (memo[s1_pos][s2_pos] != -1) {
    return memo[s1_pos][s2_pos];
  }

  if (s1[s1_pos] == s2[s2_pos]) {
    return memo[s1_pos][s2_pos] =
               edit_distance_helper(s1, s1_pos - 1, s2, s2_pos - 1, memo);
  }

  // last character is different
  // substituing
  int optimum = edit_distance_helper(s1, s1_pos - 1, s2, s2_pos - 1, memo) + 1;
  // deletion
  optimum =
      min(optimum, edit_distance_helper(s1, s1_pos - 1, s2, s2_pos, memo) + 1);
  // insertion
  optimum =
      min(optimum, edit_distance_helper(s1, s1_pos, s2, s2_pos - 1, memo) + 1);
  return memo[s1_pos][s2_pos] = optimum;
}

int edit_distance(const string &s1, const string &s2) {
  vector<vector<int>> memo(s1.size(), vector<int>(s2.size(), -1));
  return edit_distance_helper(s1, s1.size() - 1, s2, s2.size() - 1, memo);
}

int main() {
  string s1 = "Satuday";
  string s2 = "Sundays";
  cout << edit_distance(s1, s2) << endl;
}
