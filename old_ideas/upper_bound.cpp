#include <cassert>
#include <vector>

int upper_bound(const std::vector<int>& v, int value) {
    int start = 0;
    int end = v.size() - 1;

    int result = v.size();
    while (start <= end) {
        int middle = start + (end - start) / 2;

        if (v[middle] > value) {
            result = middle;
            end = middle - 1;
        } else {
            start = middle + 1;
        }
    }
    return result;
}

int main() {
    std::vector<int> v1 = {1, 2, 3, 3, 3, 5, 5, 6, 7, 7, 8, 20, 50, 111};
    assert(upper_bound(v1, 1) == 1);
    assert(upper_bound(v1, 2) == 2);
    assert(upper_bound(v1, 3) == 5);
    assert(upper_bound(v1, 7) == 10);
    assert(upper_bound(v1, 0) == 0);
    assert(upper_bound(v1, 200) == 14);
}
