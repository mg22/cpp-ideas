#include <cassert>
#include <vector>
#include <random>
#include <algorithm>
#include <iostream>

using namespace std;

int binary_search1(const vector<int>& arr, int val) {
    int left = 0;
    int right = arr.size()-1;

    while (left <= right) {
        int mid = left + (right - left) / 2;
        if (arr[mid] == val) {
            return mid;
        } else if (arr[mid] > val) {
            right = mid - 1;
        } else /* (arr[mid] < val)*/ {
            left = mid + 1;
        }
    }
    return -1;
}



int binary_search2(const vector<int>& vec, int val)
{
    int left = 0;
    int right = vec.size()-1;

    while (left <= right) {
        int mid = left + (right - left) /2;
        if (vec[mid] == val) {
            return mid;
        } else if (vec[mid] > val) {
            right = mid-1;
        } else {
            left = mid+1;
        }
    }
    return -1;
}

int binary_search3(const vector<int>& vec, int val)
{
    int lo = 0;
    int hi = vec.size();

    if (val < vec[lo]) {
        return -1;
    }
    // assert: vec[lo] <= val < vec[hi]
    while ((hi - lo) > 1) {
        int mid = (lo + hi)/2;
        if (vec[mid] <= val) {
            lo = mid;
        } else {
            // val < vec[mid]
            hi = mid;
        }
    }
    // lo+1 == hi
    return vec[lo] == val ? lo : -1;
}



constexpr int TEST_CASES = 100;
constexpr int TEST_CASE_SIZE = 10000;
constexpr int TEST_CASE_SEARCHES = 1000;
constexpr int MIN_ELEMENT = 10;
constexpr int MAX_ELEMENT = 100000;

template<typename T>
void do_tests(T bs)
{
    random_device rd;
    mt19937 re(rd());
    uniform_int_distribution<int> dist(MIN_ELEMENT, MAX_ELEMENT);

    for (int i = 0; i < TEST_CASES; ++i) {
        vector<int> vec(TEST_CASE_SIZE, 0);
        generate(vec.begin(), vec.end(), [&](){ return dist(re); });
        sort(vec.begin(), vec.end());
        
        vec.erase(unique(vec.begin(), vec.end()), vec.end());
    
        for (int s = 0; s < TEST_CASE_SEARCHES; ++s) {
            int index = uniform_int_distribution<int>(0, vec.size()-1)(re);
            int result = bs(vec, vec[index]);
            if (index != result) {
                for (auto v : vec) {
                    cout << v << " ";
                }
                cout << "\n";
                cout << "index " << index << "(" << vec[index] << ") result " << result << "(" << vec[result] << ")\n";
            }
            assert(index == result);
        }

        for (int s = 0; s < TEST_CASE_SEARCHES; ++s) {
            int element = MAX_ELEMENT+s+1;
            int result = bs(vec, element);
            assert(result == -1);
        }
        for (int s = 0; s < MIN_ELEMENT; ++s) {
            int result = bs(vec, s);
            assert(result == -1);
        }
    }
}

int main() {
    // do_tests(binary_search1);
    // do_tests(binary_search2);
    do_tests(binary_search3);
}

