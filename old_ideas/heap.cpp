#include <cassert>

#include <bits/stdc++.h>

using namespace std;

template<typename T>
class MaxHeap {
    public:
        MaxHeap() {}

        const T& Top() const {
            return tree_.front();
        }

        void Pop() {
            if (Empty()) {
                return;
            }
            tree_[0] = tree_.back();
            tree_.pop_back();
            BubbleDown_(0);
        }

        void Push(T t) {
            tree_.push_back(t);
            BubbleUp_(tree_.size()-1);
        }

        void Heapify(vector<T>&& v) {
            tree_ = move(v);
            for (ssize_t i = tree_.size()-1; i >= 0; --i) {
                BubbleDown_(i);
            }
        }

        size_t Size() const {
            return tree_.size();
        }

        bool Empty() const { return Size() == 0; }

    private:
        vector<T> tree_;

        static size_t Left_(size_t i) {
            return 2*i+1;
        }
        static size_t Right_(size_t i) {
            return 2*i+2;
        }
        static size_t Up_(size_t i) {
            return (i-1)/2;
        }

        void BubbleDown_(size_t i) {
            size_t left = Left_(i);
            if (left < tree_.size() && tree_[left] > tree_[i]) {
                swap(tree_[left], tree_[i]);
                BubbleDown_(left);
                return;
            }
            size_t right = Right_(i);
            if (right < tree_.size() && tree_[right] > tree_[i]) {
                swap(tree_[right], tree_[i]);
                BubbleDown_(right);
                return;
            }
        }

        void BubbleUp_(size_t i) {
            while (i != 0 && tree_[i] > tree_[Up_(i)]) {
                swap(tree_[i], tree_[Up_(i)]);
                i = Up_(i);
            }
        }
};

bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

void vec_print(const vector<int>& v) {
    for (int x : v) {
        cout << x << " ";
    }
    cout << "\n";
}

constexpr int TEST_CASES = 1000;
constexpr int TEST_CASE_SIZE = 1000;
constexpr int MAX_ELEMENT = 1000;


int main()
{
    random_device rd;
    mt19937_64 re(rd());
    uniform_int_distribution<int> dist(0, MAX_ELEMENT);

    for (int t = 0; t < TEST_CASES; ++t) {
        vector<int> input(TEST_CASE_SIZE, 0);
        generate(input.begin(), input.end(), [&]() { return dist(re); });

        MaxHeap<int> h;
        for (int x : input) {
            h.Push(x);
        }
        vector<int> sorted;
        while (!h.Empty()) {
            sorted.push_back(h.Top());
            h.Pop();
        }

        vector<int> expected = input;
        sort(expected.begin(), expected.end());

        vec_equal(sorted, expected);
    }

    for (int t = 0; t < TEST_CASES; ++t) {
        vector<int> input(TEST_CASE_SIZE, 0);
        generate(input.begin(), input.end(), [&]() { return dist(re); });

        MaxHeap<int> h;
        h.Heapify(vector<int>(input.begin(), input.end()));
    
        vector<int> sorted;
        while (!h.Empty()) {
            sorted.push_back(h.Top());
            h.Pop();
        }

        vector<int> expected = input;
        sort(expected.begin(), expected.end());

        vec_equal(sorted, expected);
    }
}