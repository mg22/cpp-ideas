#include <iostream>
#include <queue>
#include <vector>

using namespace std;

struct SeqIterator {
    bool operator>(const SeqIterator& other) const {
        return *current > *other.current;
    }

    vector<int>::const_iterator current, end;
};

template <typename T>
using min_priority_queue = priority_queue<T, vector<T>, greater<T>>;

vector<int> merge_sequences(const vector<vector<int>>& sequences) {
    min_priority_queue<SeqIterator> min_heap;
    for (const auto& sequence : sequences) {
        if (sequence.empty()) {
            continue;
        }
        min_heap.push({sequence.begin(), sequence.end()});
    }
    vector<int> merged;
    while (!min_heap.empty()) {
        SeqIterator top = min_heap.top();
        min_heap.pop();

        merged.push_back(*top.current);
        ++top.current;
        if (top.current != top.end) {
            min_heap.push(top);
        }
    }
    return merged;
}

void print_vector(const vector<int>& v) {
    cout << "printing vector" << endl;
    for (int x : v) {
        cout << x << " ";
    }
    cout << endl;
}

int main() {
    vector<vector<int>> sequences = {
        {1, 2, 3}, {2, 2, 4, 5, 6, 7, 8}, {1, 10, 20}};

    vector<int> merged = merge_sequences(sequences);
    print_vector(merged);
    return 0;
}
