#include "simple_array.h"

#include <iostream>
#include <memory>
#include <vector>

#include <cassert>

static constexpr int ITERATIONS = 100000;

struct Object {
    Object() {}
    Object(Object&&) {}
    ~Object() {}
    Object(const Object&) {}
    Object& operator=(Object&&) { return *this; }
    Object& operator=(const Object&) { return *this; }
    char buffer[128] = {0};
};

template<typename T>
struct ObjectWithPointer {
    ObjectWithPointer() = default;
    explicit ObjectWithPointer(const T& t) : _ptr(new T(t)) {}
    explicit ObjectWithPointer(T&& t) : _ptr(new T(std::forward<T>(t))) {}

    ~ObjectWithPointer() {
        if (_ptr) {
            delete _ptr;
        }
    }
 
    ObjectWithPointer(const ObjectWithPointer&) = delete;
    ObjectWithPointer& operator=(const ObjectWithPointer&) = delete;

    ObjectWithPointer(ObjectWithPointer&& other) : _ptr(other._ptr) {
        other._ptr = nullptr;
    }
    ObjectWithPointer& operator=(ObjectWithPointer&& other) {
        _ptr = other._ptr;
        other._ptr = nullptr;
        return *this;
    }

    T* _ptr = nullptr;

    const T& get() const { return *_ptr; }
    T& get() { return *_ptr; }
    operator bool() const {
        return _ptr == nullptr;
    }
};

template<typename Array>
void test_pod_pushback() {
    Array test;
    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test.push_back(i));
    }

    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test[i] == i);
    }
}

template<typename Array>
void test_forloop() {
    Array test;
    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test.push_back(i));
    }

    int x = 0;
    for (int y : test) {
        assert(y == x);
        ++x;
    }
}

template<typename Array>
void test_object() {
    Array test;
    Object to_copy;
    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test.push_back(to_copy));
        Object to_move;
        assert(test.push_back(std::move(to_move)));
        assert(test.emplace_back());
    }
}

template<typename Array>
void test_objectwithpointer() {
    using OI = ObjectWithPointer<int>;
    Array test;
    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test.push_back(OI(i)));
        assert(test.emplace_back(i));
    }
    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test[2*i].get() == i);
        assert(test[2*i+1].get() == i);
    }
}

template<typename Array>
void test_poormans_leak() {
    Array test;
    for (int i = 0; i < ITERATIONS; ++i) {
        assert(test.push_back(std::make_unique<int>(i)));
        assert(test.emplace_back(std::make_unique<int>(i + ITERATIONS + 1)));
    }

    for (int i = 0; i < ITERATIONS; ++i) {
        assert(*test[2*i] == i);
        assert(*test[2*i+1] == i + ITERATIONS + 1);
    }
}

int main() {
    test_pod_pushback<SimpleArray<int>>();
    test_forloop<SimpleArray<int>>();
    test_object<SimpleArray<Object>>();
    test_objectwithpointer<SimpleArray<ObjectWithPointer<int>>>();
    test_poormans_leak<SimpleArray<std::unique_ptr<int>>>();
}