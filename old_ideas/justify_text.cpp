/* EPI 28.4
 *
 * Implementation of problem 28.4. from Elements of Programming interviews book
 */

#include <iostream>
#include <string>
#include <vector>

using namespace std;

void justify_line(const vector<string> &words, size_t start, size_t end,
                  size_t spaces, vector<string> *text) {
  string line(words[start]);
  int num_words = end - start;
  if (num_words == 1) {
    line.append(spaces, ' ');
  } else {
    int base_space_count = spaces / (num_words - 1);
    int additional_spaces = spaces % (num_words - 1);
    for (size_t i = start + 1; i < end; ++i) {
      line.append(base_space_count, ' ');
      if (additional_spaces > 0) {
        line.push_back(' ');
        additional_spaces -= 1;
      }
      line.append(words[i]);
    }
  }
  text->push_back(line);
}

vector<string> justify_text(const vector<string> &words, size_t L) {
  int line_length = 0;
  int words_length = 0;
  int start_word = 0;
  vector<string> result;
  for (int i = 0; i < words.size(); ++i) {
    if (line_length + 1 + words[i].size() <= L) {
      // it fits inside, just add it
      line_length += 1 + words[i].size();
      words_length += words[i].size();
    } else {
      // no fit, add it to line
      justify_line(words, start_word, i, L - words_length, &result);
      // add new word
      start_word = i;
      line_length = words[i].size();
      words_length = line_length;
    }
  }
  // we either have one unfit word or unfinished line here
  justify_line(words, start_word, words.size(), line_length - words_length,
               &result);
  result.back().append(L - line_length, ' ');
  return result;
}

int main() {
  vector<string> words = {"The",  "quick", "brown", "fox",  "jumped",
                          "over", "the",   "lazy",  "dogs."};
  int L = 11;
  for (auto line : justify_text(words, L)) {
    cout << line << endl;
  }
}
