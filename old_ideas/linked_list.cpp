#include <cassert>

#include <bits/stdc++.h>
// #include <chrono>
// #include <iostream>
// #include <vector>
// #include <random>
// #include <algorithm>

using namespace std;

template<typename T>
class List {
    private:
        struct Node;

    public:        
        struct Iterator {
            Iterator(Node* n) : node(n) {}

            T operator*() {
                return node->val;
            }
            bool operator=(const Iterator& other) const {
                return node == other->node;
            }

            operator bool() const {
                return node != nullptr;
            }

            void operator++() {
                node = node->next;
            }

            void operator--() {
                node = node->prev;
            }

            Node* node;
        };

        List() {}
        ~List() {
            Node* current = first_;
            while (current) {
                Node* next = current->next;
                delete current;
                current = next;
            }
        }

        void PushFront(T val) {
            Node* newNode = new Node(val);
            if (Empty()) {
                first_ = newNode;
                last_ = newNode;
            } else {
                first_->prev = newNode;
                newNode->next = first_;
                first_ = newNode;
            }
            ++size_;
        }

        void PushBack(T val) {
            Node* newNode = new Node(val);
            if (Empty()) {
                first_ = newNode;
                last_ = newNode;
            } else {
                last_->next = newNode;
                newNode->prev = last_;
                last_ = newNode;
            }
            ++size_;
        }

        void PopFront() {
            // assuming non-empty list
            Node* toPop = first_;
            first_ = first_->next;
            if (first_) {
                first_->prev = nullptr;
            } else {
                last_ = nullptr;
            }
            delete toPop;
            --size_;
        }

        void PopBack() {
            // assuming non-empty list
            Node* toPop = last_;
            last_ = last_->prev;
            if (last_) {
                last_->next = nullptr;
            } else {
                first_ = nullptr;
            }
            delete toPop;
            --size_;
        }

        void Insert(Iterator it, T val) {
            if (!it) {
                PushBack(val);
                return;
            }
            if (it.node == first_) {
                PushFront(val);
                return;
            }
            // it is valid
            Node* newNode = new Node(val);
            Node* prev = it.node->prev;
            it.node->prev = newNode;
            prev->next = newNode;
            newNode->next = it.node;
            newNode->prev = prev;

            ++size_;
        }

        void Delete(Iterator it) {
            if (!it) {
                PopBack();;
                return;
            }
            if (it.node == first_) {
                PopFront();
                return;
            }
            Node* prev = it.node->prev;
            Node* next = it.node->next;
            prev->next = next;
            next->prev = prev;
            delete it.node;
            --size_;
        }

        Iterator Begin() {
            return Iterator(first_);
        }

        Iterator End() {
            return Iterator(nullptr);
        }

        T& Front() {
            // assuming non-empty list
            return first_->val;
        }

        T& Back() {
            // assuming non-empty list
            return last_->val;
        }

        size_t Size() const { return size_; }
        bool Empty() const { return Size() == 0; }

    private:
        struct Node {
            Node(T v) : val(v) {}
            T val;
            Node* next = nullptr;
            Node* prev = nullptr;
        };

        size_t size_;
        Node* first_ = nullptr;
        Node* last_ = nullptr;
};

bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

void vec_print(const vector<int>& v) {
    for (int x : v) {
        cout << x << " ";
    }
    cout << "\n";
}

int main() {
    random_device rand_dev;
    mt19937 rand_engine(rand_dev());
    uniform_int_distribution<int> input_distr(0, 100);

    {
        vector<int> vec(10000, 0);
        generate(vec.begin(), vec.end(), [&](){ return input_distr(rand_engine); });
        List<int> l;
        for (int x : vec) {
            l.PushBack(x);
        }
        vector<int> out;
        while (!l.Empty()) {
            out.push_back(l.Front());
            l.PopFront();
        }
        assert(vec_equal(vec, out));
        assert(l.Empty());
    }

    {
        vector<int> vec(10000, 0);
        generate(vec.begin(), vec.end(), [&](){ return input_distr(rand_engine); });
        List<int> l;
        for (int x : vec) {
            l.PushFront(x);
        }
        vector<int> out;
        while (!l.Empty()) {
            out.push_back(l.Back());
            l.PopBack();
        }
        assert(vec_equal(vec, out));
        assert(l.Empty());
    }

    {
        List<int> l;
        l.PushBack(1);
        l.PushBack(2);
        l.PushBack(3);
        l.PushBack(4);
        l.PushBack(5);

        assert(l.Size() == 5);

        vector<int> out;
        for (auto it = l.Begin(); it != l.End(); ++it) {
            out.push_back(*it);
        }
        vector<int> expected = {1, 2, 3, 4, 5};
        assert(vec_equal(out, expected));
    }
    
    {
        List<int> l;
        l.PushBack(1);
        l.PushBack(2);
        l.PushBack(3);
        l.PushBack(4);
        l.PushBack(5);

        assert(l.Size() == 5);

        auto it = l.Begin();
        ++it;
        ++it;
        assert(*it == 3);
        l.Insert(it, 7);

        vector<int> out;
        for (auto it = l.Begin(); it != l.End(); ++it) {
            out.push_back(*it);
        }
        vector<int> expected = {1, 2, 7, 3, 4, 5};
        assert(vec_equal(out, expected));
    }

    {
        List<int> l;
        l.PushBack(1);
        l.PushBack(2);
        l.PushBack(3);
        l.PushBack(4);
        l.PushBack(5);

        assert(l.Size() == 5);

        auto it = l.Begin();
        ++it;
        ++it;
        ++it;
        assert(*it == 4);
        l.Delete(it);

        vector<int> out;
        for (auto it = l.Begin(); it != l.End(); ++it) {
            out.push_back(*it);
        }
        vector<int> expected = {1, 2, 3, 5};
        assert(vec_equal(out, expected));
    }
}
