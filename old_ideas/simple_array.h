#pragma once

#include <cstring>
#include <cstdlib>
#include <concepts>
#include <type_traits>
#include <utility>

template<typename T>
class SimpleArray {
    public:
        using value_type = T;

        constexpr SimpleArray() = default;

        SimpleArray(SimpleArray&& other) :
            _data{std::exchange(other._data, nullptr)},
            _size{std::exchange(other._size, 0)},
            _capacity{std::exchange(other._capacity, 0)}
        {}

        ~SimpleArray() {
            delete[] _data;
        }

        //TODO:
        // move operator
        // copy constructor
        // SimpleArray(const SimpleArray& other)
        // copy operator

        bool push_back(const T& t) {
            if (!ensure(_size + 1)) {
                return false;
            }
            _data[_size] = t;
            ++_size;
            return true;
        }

        bool push_back(T&& t) {
            if (!ensure(_size + 1)) {
                return false;
            }
            _data[_size] = std::move(t);
            ++_size;
            return true;
        }

        template<typename... Ts>
        bool emplace_back(Ts&&... args) {
            if (!ensure(_size+ + 1)) {
                return false;
            }
            _data[_size] = T{std::forward<Ts>(args)...};
            ++_size;
            return true;
        }

        void clear() {
            for (size_t i = _size - 1; i < _size; --i) {
                // default construct T
                _data[i] = T();
            }
            _size = 0;
        }

        size_t size() const { return _size; }
        size_t capacity() const { return _capacity; }

        bool empty() const { return _size == 0; }
    
        T* begin() { return _data; }
        const T* begin() const { return _data; }
        T* end() { return _data + _size; }
        const T* end() const { return _data + _size; }
        
        T* data() { return _data; }
        const T* data() const { return _data; }

        T& operator[](size_t i) { return _data[i]; }
        const T& operator[](size_t i) const { return _data[i]; }

    private:
        bool ensure(size_t size) {
            if (size <= _capacity) {
                return true;
            }

            size_t new_capacity = _capacity;
            while (new_capacity < size) {
                new_capacity = (new_capacity + 1) *2;
            }
    
            T* new_data = new T[new_capacity];
            
            for (size_t i = 0; i < _size; ++i) {
                *(new_data + i) = std::move(_data[i]);
            }

            delete[] _data;

            _data = new_data;
            _capacity = new_capacity;
            return true;
        }

        T* _data = nullptr;
        size_t _capacity = 0;
        size_t _size = 0;
};

//TODO: malloc, free, default construct
template<typename T>
class MallocArray {
    public:
        using value_type = T;

        constexpr MallocArray() = default;

        MallocArray(MallocArray&& other) :
            _data{std::exchange(other._data, nullptr)},
            _size{std::exchange(other._size, 0)},
            _capacity{std::exchange(other._capacity, 0)}
        {}

        ~MallocArray() {
            delete[] _data;
        }

        //TODO:
        // move operator
        // copy constructor
        // MallocArray(const MallocArray& other)
        // copy operator

        bool push_back(const T& t) {
            if (!ensure(_size + 1)) {
                return false;
            }
            _data[_size] = t;
            ++_size;
            return true;
        }

        bool push_back(T&& t) {
            if (!ensure(_size + 1)) {
                return false;
            }
            _data[_size] = std::move(t);
            ++_size;
            return true;
        }

        template<typename... Ts>
        bool emplace_back(Ts&&... args) {
            if (!ensure(_size+ + 1)) {
                return false;
            }
            _data[_size] = T{std::forward<Ts>(args)...};
            ++_size;
            return true;
        }

        void clear() {
            for (size_t i = _size - 1; i < _size; --i) {
                // default construct T
                _data[i] = T();
            }
            _size = 0;
        }

        size_t size() const { return _size; }
        size_t capacity() const { return _capacity; }

        bool empty() const { return _size == 0; }
    
        T* begin() { return _data; }
        const T* begin() const { return _data; }
        T* end() { return _data + _size; }
        const T* end() const { return _data + _size; }
        
        T* data() { return _data; }
        const T* data() const { return _data; }

        T& operator[](size_t i) { return _data[i]; }
        const T& operator[](size_t i) const { return _data[i]; }

    private:
        bool ensure(size_t size) {
            if (size <= _capacity) {
                return true;
            }

            size_t new_capacity = _capacity;
            while (new_capacity < size) {
                new_capacity = (new_capacity + 1) *2;
            }
    
            T* new_data = new T[new_capacity];
            
            for (size_t i = 0; i < _size; ++i) {
                *(new_data + i) = std::move(_data[i]);
            }

            delete[] _data;

            _data = new_data;
            _capacity = new_capacity;
            return true;
        }

        T* _data = nullptr;
        size_t _capacity = 0;
        size_t _size = 0;
};