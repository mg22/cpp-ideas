#include <cassert>

#include <bits/stdc++.h>
using namespace std;

class Fenwick {
    public:
        Fenwick(int n) : n_(n), tree_(n+1, 0)
        {}

        void Add(int index, int val) {
            while (index <= n_) {
                tree_[index] += val;
                index = index + (index & -index);
            }
        }

        // sum of 1..index
        int PrefixSum(int index) {
            int result = 0;
            while (index > 0) {
                result += tree_[index];
                index = index & (index - 1);
            }
            return result;
        }

    private:
        int n_;
        vector<int> tree_;
};

bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}


int main()
{
    mt19937_64 mt((random_device())());
    uniform_int_distribution<int> dist(10, 10000);

    for (int t = 0; t < 100; ++t) {
        constexpr int EXPECTED_SIZE = 10000;
        Fenwick fenwick(EXPECTED_SIZE);

        vector<int> prefixSums(EXPECTED_SIZE, 0);
        for (int tt = 0; tt < 20; ++tt) {
            vector<int> expected(EXPECTED_SIZE, 0);
            generate(expected.begin(), expected.end(), [&]() { return dist(mt); });

            int rollingSum = 0;
            for (size_t i = 0; i < expected.size(); ++i) {
                fenwick.Add(i+1, expected[i]);
                rollingSum += expected[i];
                prefixSums[i] += rollingSum;
            }

            for (size_t i = 0; i < prefixSums.size(); ++i) {
                assert(fenwick.PrefixSum(i+1) == prefixSums[i]);
            }
        }

    }
}