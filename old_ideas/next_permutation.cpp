#include <bits/stdc++.h>

#include <cassert>

using namespace std;

template <typename It>
bool my_next_permutation(It begin, It end) {
    if ((end - begin) < 2) {
        return false;
    }
    It suffix = end - 1;
    while (suffix != begin && *(suffix - 1) >= *suffix) {
        --suffix;
    }
    if (suffix == begin) {
        return false;
    }

    It toInsert = suffix - 1;
    It smaller = suffix;
    while (smaller != end && *smaller >= *toInsert) {
        ++smaller;
    }
    swap(*(smaller - 1), *toInsert);
    reverse(suffix, end);
    return true;
}

template <typename It>
void vec_print(It begin, It end) {
    while (begin != end) {
        cout << *begin << " ";
        ++begin;
    }
    cout << "\n";
}

bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (size_t i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

int main() {
    constexpr int TESTCASES = 1;
    constexpr int TESTSIZE = 10;

    for (int t = 0; t < TESTCASES; ++t) {
        vector<int> test;
        for (int i = 0; i < TESTSIZE; ++i) {
            test.push_back(i + 1);
        }
        vector<int> expected = test;

        vector<vector<int>> my_perms;
        // vec_print(test.begin(), test.end());
        while (my_next_permutation(test.begin(), test.end())) {
            // vec_print(test.begin(), test.end());
            my_perms.push_back(test);
        }

        // cout << "----\n";
        vector<vector<int>> expected_perms;
        // vec_print(expected.begin(), expected.end());
        while (next_permutation(expected.begin(), expected.end())) {
            expected_perms.push_back(expected);
            // vec_print(expected.begin(), expected.end());
        }

        assert(my_perms.size() == expected_perms.size());
        for (size_t i = 0; i < my_perms.size(); ++i) {
            assert(vec_equal(my_perms[i], expected_perms[i]));
        }
    }
}
