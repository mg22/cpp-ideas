#include <cassert>

#include <chrono>
#include <iostream>
#include <vector>
#include <random>
#include <algorithm>

using namespace std;

template<typename I>
void merge_halves(I begin, I mid, I end) {
    vector<typename I::value_type> tmp;
    I s1 = begin;
    I s2 = mid;

    while (s1 != mid && s2 != end) {
        if (*s1 < *s2) {
            tmp.push_back(*s1);
            ++s1;
        } else {
            tmp.push_back(*s2);
            ++s2;
        }
    }

    while (s1 != mid) {
        tmp.push_back(*s1);
        ++s1;
    }
    while (s2 != end) {
        tmp.push_back(*s2);
        ++s2;
    }

    for (auto& t : tmp) {
        *begin = t;
        ++begin;
    }
}

template<typename I>
void merge_sort(I begin, I end) {
    if ((end - begin) <= 1) {
        return;
    }
    I mid = begin + (end - begin) / 2;

    merge_sort(begin, mid);
    merge_sort(mid, end);
    merge_halves(begin, mid, end);
}

bool vec_equal(const vector<int>& v1, const vector<int> v2) {
    if (v1.size() != v2.size()) {
        return false;
    }
    for (int i = 0; i < v1.size(); ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

int main() {
    random_device rand_dev;
    mt19937 rand_engine(rand_dev());
    uniform_int_distribution<int> input_distr(0, 10000000);

    for (int i = 0; i < 100; ++i) {
        vector<int> vec(1000000, 0);
        generate(vec.begin(), vec.end(), [&](){ return input_distr(rand_engine); });
        

        vector<int> expected = vec;
        auto t1 = chrono::high_resolution_clock::now();
        sort(expected.begin(), expected.end());
        auto t2 = chrono::high_resolution_clock::now();

        auto t3 = chrono::high_resolution_clock::now();
        merge_sort(vec.begin(), vec.end());
        auto t4 = chrono::high_resolution_clock::now();
        
        assert(vec_equal(vec, expected) == true);

        chrono::duration<double> elapsed_std = t2 - t1;
        chrono::duration<double> elapsed_merge = t4 - t3;
        std::cout << "std::sort: " << elapsed_std.count() << "\n";
        std::cout << "merge_sort: " << elapsed_merge.count() << "\n";
    }
}