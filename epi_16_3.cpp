#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

int numTraversalsHelper(int r, int c, int rows, int cols, vector<vector<int>>& memo) {
    if (r == rows && c == cols) {
        return 1;
    }
    if (memo[r][c] != -1) {
        return memo[r][c];
    }
    // right
    int right = 0;
    int down = 0;
    if (c < cols) {
        right = numTraversalsHelper(r, c+1, rows, cols, memo);
    }
    if (r < rows) {
        down = numTraversalsHelper(r+1, c, rows, cols, memo);
    }
    return memo[r][c] = right + down;
}

int NumTraversals(int rows, int cols) {
    vector<vector<int>> memo(rows+1, vector<int>(cols+1, -1));
    return numTraversalsHelper(0, 0, rows, cols, memo);
}

TEST_CASE("EPI_16_3: Basic tests", "[EPI_16_3]") {
    REQUIRE( NumTraversals(0, 0) == 1 );
    REQUIRE( NumTraversals(1, 0) == 1 );
    REQUIRE( NumTraversals(0, 1) == 1 );
    REQUIRE( NumTraversals(1, 1) == 2 );
    REQUIRE( NumTraversals(4, 4) == 70 );
    REQUIRE( NumTraversals(4, 3) == 35 );
    REQUIRE( NumTraversals(3, 3) == 20 );
}