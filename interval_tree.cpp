#include <catch2/catch.hpp>
#include <cassert>
#include <vector>
#include <random>
#include <algorithm>
#include <limits>

/// Tree holding interval [0..n)
///
/// operations:
///     * Set(index, val) in O(log n)
///     * Sum(start, end) in O(log n)
///     * Multiply(start, end, val) in O(log n)
///
class IntervalTreeMemory {
    public:
        /// size is at least 1
        IntervalTreeMemory(int size) {
            _root = new Node(0, size);
        }
        ~IntervalTreeMemory() {
            delete _root;
        }

        void Set(int index, int value) {
            _root->Set(index, value);
        }

        /// [start, end)
        int Sum(int start, int end) {
            return _root->Sum(start, end);
        }

        /// [start, end)
        void Multiply(int start, int end, int multiplier) {
            _root->Multiply(start, end, multiplier);
        }

    private:
        struct Node {
            Node(int start, int end) :
                _value(0), _multiplier(1),
                _start(start), _end(end), _left(nullptr), _right(nullptr)
            {
                if ((_end - _start) == 1) {
                    return;
                }
                int mid = (_start + _end)/2;
                _left = new Node(_start, mid);
                _right = new Node(mid, _end); 
            }
            ~Node() {
                delete _left;
                delete _right;
            }

            void Set(int index, int value) {
                if (Leaf()) {
                    _value = value;
                    return;
                }
                PushMultiplier();
                int mid = (_start + _end)/2;
                if (index < mid) {
                    _left->Set(index, value);
                } else {
                    _right->Set(index, value);
                }
                _value = _left->_value + _right->_value;
            }

            void Multiply(int start, int end, int multiplier) {
                // I am outside
                if (_start >= end || _end <= start) {
                    return;
                }
                // I am whole inside
                if (start <= _start && _end <= end) {
                    _multiplier *= multiplier;
                    _value *= multiplier;
                    return;
                }
                PushMultiplier();
                _left->Multiply(start, end, multiplier);
                _right->Multiply(start, end, multiplier);
                _value = _left->_value + _right->_value;
            }

            int Sum(int start, int end) {
                // I am outside
                if (_start >= end || _end <= start) {
                    return 0;
                }
                // I am whole inside
                if (start <= _start && _end <= end) {
                    return _value;
                }
                PushMultiplier();
                // part is inside, part is not
                return _left->Sum(start, end) + _right->Sum(start, end);
            }


            bool Leaf() const {
                return (_end - _start) == 1;
            }

            void PushMultiplier() {
                if (Leaf() || _multiplier == 1) return;

                _left->_multiplier *= _multiplier;
                _left->_value *= _multiplier;
                _right->_multiplier *= _multiplier;
                _right->_value *= _multiplier;
                _multiplier = 1;
            }

            int _value;
            int _multiplier;

            int _start, _end;
            Node* _left;
            Node* _right;
        };

        Node* _root;
};

// TODO later
// class IntervalTreeArray {
//     public:
    
// };

// TODO: make it parameteric, also for IntervalTreeArray
TEST_CASE( "IntervalTreeMemory: Sanity check", "[IntervalTree]" ) {
    
    IntervalTreeMemory it(10);

    // i: 0 1 2 3 4 5 6 7 8 9
    // v: 0 0 0 0 0 0 0 0 0 0
    it.Set(2, 1);
    it.Set(3, 2);
    it.Set(4, 3);
    it.Set(7, 1);
    it.Set(9, 1);
    // i: 0 1 2 3 4 5 6 7 8 9
    // v: 0 0 1 2 3 0 0 1 0 1

    SECTION( "sums are correct" ) {
        REQUIRE( it.Sum(0, 3) == 1 );
        REQUIRE( it.Sum(3, 4) == 2 );
        REQUIRE( it.Sum(3, 5) == 5 );
        REQUIRE( it.Sum(5, 7) == 0 );
        REQUIRE( it.Sum(9, 10) == 1 );
        REQUIRE( it.Sum(0, 10) == 8 );
    }

    SECTION( "clear everything" ) {
        it.Set(2, 0);
        it.Set(3, 0);
        it.Set(4, 0);
        it.Set(7, 0);
        it.Set(9, 0);
        // i: 0 1 2 3 4 5 6 7 8 9
        // v: 0 0 0 0 0 0 0 0 0 0

        REQUIRE( it.Sum(0, 10) == 0 );
    }

    SECTION( "changing values work" ) {
        it.Set(4, 9);
        // i: 0 1 2 3 4 5 6 7 8 9
        // v: 0 0 1 2 9 0 0 1 0 1

        REQUIRE( it.Sum(4, 5) == 9 );
        REQUIRE( it.Sum(0, 5) == 12 );
        REQUIRE( it.Sum(4, 10) == 11 );

        it.Set(0, 20);
        it.Set(9, 20);
        // i: 0  1 2 3 4 5 6 7 8 9
        // v: 20 0 1 2 9 0 0 1 0 20
        
        REQUIRE( it.Sum(0, 10) == 53 );
        REQUIRE( it.Sum(0, 1) == 20 );
        REQUIRE( it.Sum(9, 10) == 20 );
    } 

    SECTION( "empty intervals" ) {
        REQUIRE( it.Sum(0, 0) == 0 );
        REQUIRE( it.Sum(5, 5) == 0 );
        REQUIRE( it.Sum(10, 10) == 0 );
    }

    SECTION ( "multiplication works" ) {
        // starting state:
        // i: 0 1 2 3 4 5 6 7 8 9
        // v: 0 0 1 2 3 0 0 1 0 1

        SECTION ( "" ) {
            it.Multiply(0, 10, 2);
            REQUIRE( it.Sum(0, 10) == 16 );
        }

        SECTION ( "" ) {
            it.Multiply(0, 10, 2);
            REQUIRE( it.Sum(0, 10) == 16 );
        }
    }
}

TEST_CASE( "IntervalTreeMemory: Randomized operations", "[IntervalTree]" ) {
    // TODO
}

/// Fenwick tree
class Fenwick {
    public:
        Fenwick(int size) : _n(size), _vec(_n+1, 0) {}

        // array[index] += value
        void Add(int index, int value) {
            while (index <= _n) {
                _vec[index] += value;
                index = index + (index & -index);
            }
        }
        
        // sum [1..index]
        int PrefixSum(int index) {
            int result = 0;
            while (index) {
                result += _vec[index];
                index = index & index-1;
            }
            return result;
        }

    private:
        int _n;
        std::vector<int> _vec;
};

TEST_CASE( "FenwickTree: Sanity check", "[FenwickTree]" ) {

    // i: 1 2 3 4 5 6 7 8 9 10
    // v: 0 0 0 0 0 0 0 0 0 0
    Fenwick fw(10);

    for (int i = 1; i <= 10; ++i) {
        REQUIRE( fw.PrefixSum(i) == 0 );
    }

    SECTION( "" ) {
        fw.Add(1, 1);
        fw.Add(2, 1);
        fw.Add(3, 1);
        fw.Add(4, 1);
        fw.Add(5, 1);
        fw.Add(6, 1);
        fw.Add(7, 1);
        fw.Add(8, 1);
        fw.Add(9, 1);
        fw.Add(10, 1);
        // i: 1 2 3 4 5 6 7 8 9 10
        // v: 1 1 1 1 1 1 1 1 1 1

        for (int i = 1; i <= 10; ++i) {
            REQUIRE( fw.PrefixSum(i) == i );
        }

        SECTION( "" ) {
            fw.Add(1, -1);
            REQUIRE( fw.PrefixSum(10) == 9 );
        }
    }

    SECTION( "" ) {
        fw.Add(5, 1);
        // i: 1 2 3 4 5 6 7 8 9 10
        // v: 0 0 0 0 1 0 0 0 0 0
        REQUIRE( fw.PrefixSum(4) == 0 );
        REQUIRE( fw.PrefixSum(5) == 1 );
        REQUIRE( fw.PrefixSum(6) == 1 );
    }
}