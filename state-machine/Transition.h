#pragma once

template<class State, class Context>
class Transition {
public:
    virtual ~Transition() {}

    virtual State get_state() = 0;    
    virtual State call(Context &context) = 0;
};
