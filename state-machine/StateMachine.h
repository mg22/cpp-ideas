#pragma once

#include <unordered_map>

#include "Transition.h"

template<class State, class Context>
class StateMachine {
public:
    ~StateMachine()
    {
        for (auto it = transitions.begin(); it != transitions.end(); ++it) {
            delete (*it).second;
        }
    }


    void add(Transition<State,Context> *transition)
    {
        transitions[transition->get_state()] = transition;
    }
    
    void set_state(State state)
    {
        current_state = state;   
    }
    
    void step(Context &context)
    {
        if (transitions.count(current_state) == 0) {
            // no transition for current state
            // do nothing
            // TODO: something better? assert/exit?
            return;
        }
        current_state = transitions[current_state]->call(context);
    }

private:
    std::unordered_map<State,Transition<State,Context>*> transitions;
    State current_state;
};
