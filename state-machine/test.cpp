#include <iostream>
using namespace std;

#include "StateMachine.h"
#include "Transition.h"

enum class State {
    YES,
    NO,
};

// needs to be done to define hash for State
namespace std {
template<>
struct hash<State> {
    size_t operator()(const State& v) const {
        return hash<int>()(static_cast<int>(v));
    }
};
}

class Context {};

class YesToNo : public Transition<State, Context> {
    State get_state() {
        return State::YES;
    }
    State call(Context &context) {
        (void) context;
        cout << "Yes to No!" << endl;
        return State::NO;
    }
};

class NoToYes : public Transition<State, Context> {
    State get_state() {
        return State::NO;
    }
    State call(Context &context) {
        (void) context;
        cout << "No to Yes!" << endl;
        return State::YES;
    }
};

int main() {
    StateMachine<State,Context> sm;
    sm.set_state(State::YES);
    sm.add(new YesToNo());
    sm.add(new NoToYes());
    Context c;
    sm.step(c);
    sm.step(c);
    sm.step(c);
    return 0;
}
