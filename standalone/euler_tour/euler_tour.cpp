#include <bits/stdc++.h>
using namespace std;


class Graph {
public:
  Graph(int size) : graph_(size) {}

  void AddEdge(int v, int w) {
    graph_[v][w] = 0;
    graph_[w][v] = 0;
  }

  bool IsEulerian() const {
    // all degrees needs to be even
    for (const auto& neighbours : graph_) {
      if (neighbours.size() % 2 != 0) {
        return false;
      }
    }
    return true;
  }

  // assumes graph is connected
  vector<pair<int,int>> GetEulerTour() {
    if (!IsEulerian()) {
      return {};
    }

    ClearMarks();
    vector<pair<int,int>> tour;
    Dfs(0, tour);
    return tour;
  }

private:
  void MarkEdge(int v, int w) {
    graph_[v][w] = 1;
    graph_[w][v] = 1;
  }

  void ClearMarks() {
    for (auto& neighbours : graph_) {
      for (auto& n : neighbours) {
        n.second = 0;
      }
    }
  }
  
  void Dfs(int current, vector<pair<int,int>>& tour) {
    for (auto& neighbour : graph_[current]) {
      if (neighbour.second == 0) {
        MarkEdge(current, neighbour.first);
        Dfs(neighbour.first, tour);
        tour.push_back({current, neighbour.first});
      }
    }
  }

  vector<unordered_map<int,int>> graph_;
};

int main() {
  // input format:
  // - first line 2 numbers n, k
  // - graph with n vertices 1..n and k edges
  // - graph is undirected, connected
  // - next k lines 2 numbers v, w (fron 1 to n): edge between v and w

  // load input
  int n, k; cin >> n >> k;
  Graph graph(n);
  for (int i = 0; i < k; ++i) {
    int v, w;
    cin >> v >> w;
    v--; w--;
    graph.AddEdge(v, w);
  }

  // find tour
  auto tour = graph.GetEulerTour();

  // print result
  for (auto& edge : tour) {
    cout << min(edge.first, edge.second)+1 << " " << max(edge.first, edge.second)+1 << "\n";
  }
}