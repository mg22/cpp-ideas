#include <bits/stdc++.h>
using namespace std;

class Graph {
public:
  Graph(int size) : size_(size), adj_(size_), degree_(size_, 0) {}

  void AddEdge(int e1, int e2) {
    adj_[e1].push_back(e2);
    adj_[e2].push_back(e1);
    degree_[e1]++;
    degree_[e2]++;
  }

  vector<int> ConstructGreedyColoring() {
    int next_color = 1;
    vector<int> coloring(size_, -1);

    vector<pair<int,int>> degrees;
    for (int i = 0; i < size_; ++i) {
      degrees.push_back({degree_[i], i});
    }
    sort(degrees.begin(), degrees.end());

    for (int i = 0; i < size_; ++i) {
      int current = degrees[size_-i-1].second;
      unordered_set<int> adjecent_colors;
      for (int j = 0; j < adj_[current].size(); ++j) {
        int neighbor = adj_[current][j];
        if (coloring[neighbor] == -1) {
          continue;
        } else {
          adjecent_colors.insert(coloring[neighbor]);
        }
      }
      for (int c = 1; c < next_color; ++c) {
        if (adjecent_colors.count(c) == 0) {
          coloring[current] = c;
          break;
        }
      }
      if (coloring[current] == -1) {
        coloring[current] = next_color;
        ++next_color;
      }
    }

    return coloring;
  }

private:
  int size_;
  vector<vector<int>> adj_;
  vector<int> degree_;
};

int main() {
  // input format:
  // - first line 2 numbers n, k
  // - graph with n vertices 1..n and k edges
  // - graph is undirected, connected
  // - next k lines 2 numbers v, w (fron 1 to n): edge between v and w

  // load input
  int n, k; cin >> n >> k;
  Graph graph(n);
  for (int i = 0; i < k; ++i) {
    int v, w;
    cin >> v >> w;
    v--; w--;
    graph.AddEdge(v, w);
  }

  // find coloring
  auto coloring = graph.ConstructGreedyColoring();

  // print result
  for (int i = 0; i < coloring.size(); ++i) {
    cout << "vertex: " << i+1 << " color: " << coloring[i] << "\n";
  }
}