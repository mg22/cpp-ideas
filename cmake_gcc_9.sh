#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir ${SCRIPT_DIR}/build
cd ${SCRIPT_DIR}/build

CC=gcc-9 CXX=g++-9 cmake -DCMAKE_BUILD_TYPE=Debug ../