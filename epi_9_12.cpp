#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

struct Node {
  unique_ptr<Node> left;
  unique_ptr<Node> right;
  int val = -1;
};

unique_ptr<Node> ReconstructHelper(
  const vector<int>& inorder, const  vector<int>& preorder, unordered_map<int,int>& inorder_pos, int inorder_start, int inorder_end, int preorder_start
) {
  auto result = make_unique<Node>();
  int root = preorder[preorder_start];
  result->val = root;
  int root_pos = inorder_pos[root];
  int left_tree = root_pos - inorder_start;
  int right_tree = inorder_end - (root_pos+1);
  if (left_tree >= 1) {
    result->left = ReconstructHelper(inorder, preorder, inorder_pos, inorder_start, root_pos, preorder_start+1);
  }
  if (right_tree >= 1) {
    result->right = ReconstructHelper(inorder, preorder, inorder_pos, root_pos+1, inorder_end, preorder_start+1+left_tree);
  }
  return result;
}

unique_ptr<Node> Reconstruct(const vector<int>& inorder, const vector<int>& preorder) {
  unordered_map<int,int> inorder_pos;
  for (size_t i = 0; i < inorder.size(); ++i) {
    inorder_pos[inorder[i]] = i;
  }

  return ReconstructHelper(inorder, preorder, inorder_pos, 0, inorder.size(), 0);
}

void ensure_size(int row, vector<string>& buffer) {
  if (row >= buffer.size()) {
    int to_size = row - buffer.size();
    for (int i = 0; i <= to_size; ++i) {
      buffer.push_back("");
    }
  }
}

void draw_tree(Node* root) {
  struct NodeLevel {
    Node* node; int level;
  };
  int prev_level = 1;
  queue<NodeLevel> work;
  work.push({root, 1});

  while (!work.empty()) {
    auto top = work.front(); work.pop();
    if (top.level != prev_level) {
      prev_level = top.level;
      // cout << '\n';
    }
    if (top.node) {
      // cout << static_cast<char>('A' + top.node->val -1);
      work.push({top.node->left.get(), top.level+1});
      work.push({top.node->right.get(), top.level+1});
    } else {
      // cout << 'x';
    }
  }
  // cout << '\n';
}

TEST_CASE("EPI_9_12: Basic tests", "[EPI_9_12]") {
  vector<int> inorder = {6, 2, 1, 5, 8, 3, 4, 9, 7};
  vector<int> preorder = {8, 2, 6, 5, 1, 3, 4, 7, 9};

  auto res = Reconstruct(inorder, preorder);

  REQUIRE( res != nullptr );

  draw_tree(res.get());
}