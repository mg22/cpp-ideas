#include <catch2/catch.hpp>

#include <bits/stdc++.h>

class Treap {
public:
    Treap() :
        rand_(std::random_device{}())
    {}
    ~Treap() {
        Delete(root_);
    }

    void Insert(int key) {
        Node* new_node = new Node(key, priority_dist_(rand_));
        InsertHelper(root_, new_node);
    }

    void Erase(int key) {
        if (!root_) return;
        EraseHelper(root_, key);
    }

    bool Find(int key) {
        return FindHelper(root_, key);
    }

private:
    struct Node {
        Node(int _key, uint64_t _priority) : key(_key), priority(_priority) {}
        int key;
        uint64_t priority;
        Node* left = nullptr;
        Node* right = nullptr;
    };

    static void Delete(Node* root) {
        if (!root) return;
        Delete(root->left);
        Delete(root->right);
        delete root;
    }

    static void EraseHelper(Node*& root, int key) {
        if (root->key == key) {
            Node* old = root;
            Merge(root->left, root->right, root);
            delete old;
        } else {
            EraseHelper((key < root->key) ? root->left : root->right, key);
        }
    }

    static Node* FindHelper(Node* root, int key) {
        while (root && root->key != key) {
            if (key < root->key) {
                root = root->left;
            } else {
                root = root->right;
            }
        }
        return root;
    }

    static void InsertHelper(Node*& root, Node* to_insert) {
        if (!root) {
            root = to_insert;
            return;
        }
        if (to_insert->priority < root->priority) {
            // new_node is new root
            Split(root, to_insert->key, to_insert->left, to_insert->right);
            root = to_insert;
        } else {
            if (to_insert->key < root->key) {
                InsertHelper(root->left, to_insert);
            } else {
                InsertHelper(root->right, to_insert);
            }
        }
    }

    static void Split(Node* root, int key, Node*& left, Node*& right) {
        if (!root) {
            left = right = nullptr;
        } else if (root->key < key) {
            Split(root->right, key, root->right, right);
            left = root;
        } else {
            Split(root->left, key, left, root->left);
            right = root;
        }
    }

    static void Merge(Node* left, Node* right, Node*& out) {
        if (!left || !right) {
            out = left ? left : right;
            return;
        }
        if (left->priority < right->priority) {
            // left is new root
            Merge(left->right, right, left->right);
            out = left;
        } else {
            // right is new root
            Merge(left, right->left, right->left);
            out = right;
        }
    }

    Node* root_ = nullptr;

    std::default_random_engine rand_;
    std::uniform_int_distribution<uint64_t> priority_dist_;
};



TEST_CASE("Treap: Sanity check", "[Treap]") {
    Treap treap;

    treap.Insert(1);
    treap.Insert(2);
    treap.Insert(10);
    treap.Insert(42);
    treap.Insert(47);
    treap.Insert(1000);

    REQUIRE( treap.Find(1) );
    REQUIRE( treap.Find(2) );
    REQUIRE( treap.Find(10) );
    REQUIRE( treap.Find(42) );
    REQUIRE( treap.Find(47) );
    REQUIRE( treap.Find(1000) );

    SECTION( "erase existing ") {
        treap.Erase(10);
        REQUIRE( treap.Find(10) == false );
    }

    SECTION( "insert new " ) {
        REQUIRE( treap.Find(44) == false );
        treap.Insert(44);
        REQUIRE( treap.Find(44) == true );
    }

    SECTION( "erases and inserts" ) {
        treap.Erase(2);
        REQUIRE( treap.Find(2) == false );
        treap.Erase(1000);
        REQUIRE( treap.Find(1000) == false );

        treap.Insert(2);
        REQUIRE( treap.Find(2) == true );
    }

    SECTION( "erase everything" ) {
        treap.Erase(1);
        treap.Erase(2);
        treap.Erase(10);
        treap.Erase(42);
        treap.Erase(47);
        treap.Erase(1000);

        REQUIRE( treap.Find(1) == false );
        REQUIRE( treap.Find(2) == false );
        REQUIRE( treap.Find(10) == false );
        REQUIRE( treap.Find(42) == false );
        REQUIRE( treap.Find(47) == false );
        REQUIRE( treap.Find(1000) == false );
    }
}

TEST_CASE("Treap: Random insert", "[Treap]") {
    //todo
}