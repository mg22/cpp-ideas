#include <catch2/catch.hpp>
#include <cassert>
#include <vector>
#include <random>
#include <algorithm>
#include <limits>

size_t binary_search_1(const std::vector<int>& arr, size_t left, size_t right, int val) {
    // [left, right]
    while (left <= right) {
        size_t mid = left + (right-left)/2;
        if (arr[mid] == val) {
            return mid;
        } else if (arr[mid] < val) {
            left = mid+1;
        } else {
            right = mid-1;
        }
    }
    return std::numeric_limits<size_t>::max();
}


TEST_CASE("BinarySearch1: Can find", "[BinarySearch]") {

    std::default_random_engine re(std::random_device{}());
    std::uniform_int_distribution<> gen_range(1, 10000);

    for (int i = 0; i < 100; ++i) {
        std::vector<int> v;
        for (int j = 0; j < 1000; ++j) {
            v.push_back(gen_range(re));
        }
        std::sort(v.begin(), v.end());
        std::uniform_int_distribution<size_t> query_range(0, 1000-1);
        for (int j = 0; j < 1000; ++j) {
            size_t q = query_range(re);
            int exp = *std::lower_bound(v.begin(), v.end(), v[q]);
            int my = v[binary_search_1(v, 0, v.size()-1, v[q])];

            REQUIRE( exp == my );
        }
    }
}
