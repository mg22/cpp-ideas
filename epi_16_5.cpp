#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

static const vector<pair<int,int>> MOVES = {
  {1, 0}, {-1, 0}, {0, 1}, {0, -1}
};

bool occursHelper(int r, int c, int p, const vector<vector<int>>& array, const vector<int>& pattern, vector<vector<vector<int>>>& memo) {
  if (p == pattern.size()) {
    return true;
  }
  if (r < 0 || r >= array.size() || c < 0 || c >= array[r].size()) {
    return false;
  }
  if (memo[r][c][p] != -1) {
    return memo[r][c][p];
  }

  int result = 0;
  if (array[r][c] == pattern[p]) {
    for (const auto& move : MOVES) {
      int nr = r + move.first;
      int nc = c + move.second;
      if (occursHelper(nr, nc, p+1, array, pattern, memo)) {
        result = 1;
        break;
      }
    }
  }

  return memo[r][c][p] = result;
}

bool Occurs(const vector<vector<int>>& array, const vector<int>& pattern) {
  vector<vector<vector<int>>> memo(array.size(), vector<vector<int>>(array[0].size(), vector<int>(pattern.size(), -1)));
  for (int r = 0; r < array.size(); ++r) {
    for (int c = 0; c < array[r].size(); ++c) {
      if (occursHelper(r, c, 0, array, pattern, memo)) {
        return true;
      }
    }
  }
  return false;
}

TEST_CASE("EPI_16_5: Basic tests", "[EPI_16_5]") {
  REQUIRE( Occurs({{1,2,3}, {3,4,5}, {5,6,7}}, {1,3,4,6}) == true );
  REQUIRE( Occurs({{1,2,3}, {3,4,5}, {5,6,7}}, {1,2,3,4}) == false );
  REQUIRE( Occurs({{1}}, {1}) == true );
  REQUIRE( Occurs({{1}}, {1,1,1,1}) == false );
  REQUIRE( Occurs({{1}, {2}}, {1,2,3,4}) == false );
  REQUIRE( Occurs({{1}, {2}}, {1,2,1,2}) == true );
}