#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

void SortKSortedSeq(vector<int>& seq, int k) {
  priority_queue<int, vector<int>, greater<int>> min_heap;
  for (int i = 0; i < k+1; ++i) {
    min_heap.push(seq[i]);
  }

  vector<int> result;
  
  for (int i = k+1; i < seq.size(); ++i) {
    result.push_back(min_heap.top());
    min_heap.pop();
    min_heap.push(seq[i]);
  }

  while (!min_heap.empty()) {
    result.push_back(min_heap.top());
    min_heap.pop();
  }

  seq = result;
}

struct Star {
  int x, y, z;

  bool operator<(const Star& other) const {
    int this_dist_2 = x*x + y*y + z*z;
    int other_dist_2 = other.x*other.x + other.y*other.y + other.z*other.z;
    return this_dist_2 < other_dist_2;
  };
};

TEST_CASE("EPI_10_3: Basic tests", "[EPI_10_3]") {
  vector<int> v = {3, -1, 2, 6, 4, 5, 8};
  auto vv = v;
  sort(vv.begin(), vv.end());

  SortKSortedSeq(v, 2);

  REQUIRE( v == vv );
  
}