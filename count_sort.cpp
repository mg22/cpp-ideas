#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

// assuming to_sort elements are [0,max_val)
void count_sort(vector<int>& to_sort, int max_val) {
  vector<int> counts(max_val, 0);
  for (int n : to_sort) {
    counts[n]++;
  }
  int next_pos = 0;
  for (int i = 0; i < counts.size(); ++i) {
    for (int j = next_pos; j < next_pos + counts[i]; ++j) {
      to_sort[j] = i;
    }
    next_pos += counts[i];
  }
}


TEST_CASE("Count sort: Basic tests", "[Count sort]") {
  vector<int> v = {2, 1, 5, 7, 7, 30, 50, 22};
  vector<int> s = v;
  sort(s.begin(), s.end());
  count_sort(v, 51);
  REQUIRE( v == s );
}

TEST_CASE("Count sort: Random tests", "[Count sort]") {
  constexpr int TEST_CASES = 200;
  constexpr int TEST_CASE_SIZE = 100000;
  constexpr int MIN_ELEMENT = 100;
  constexpr int MAX_ELEMENT = 1000;

  std::default_random_engine re(std::random_device{}());
  std::uniform_int_distribution<int> el_dist(MIN_ELEMENT, MAX_ELEMENT);

  for (int t = 0; t < TEST_CASES; ++t) {
    std::vector<int> to_test; to_test.reserve(TEST_CASE_SIZE);
    for (int i = 0; i < TEST_CASE_SIZE; ++i) {
      to_test.push_back(el_dist(re));
    }
    auto sorted = to_test;
    std::sort(sorted.begin(), sorted.end());
    count_sort(to_test, MAX_ELEMENT+1);
    REQUIRE( to_test == sorted );
  }
}