#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

vector<vector<int>> SubsetsOfK(const vector<int>& input, int k) {
  vector<int> mask; mask.reserve(input.size());
  for (int i = 0; i < input.size(); ++i) {
    if (i < (input.size() - k)) {
      mask.push_back(0);
    } else {
      mask.push_back(1);
    }
  }

  vector<vector<int>> result;
  do {
    result.push_back({});
    result.back().reserve(k);
    for (int i = 0; i < input.size(); ++i) {
      if (mask[i]) {
        result.back().push_back(input[i]);
      }
    }
  } while (next_permutation(mask.begin(), mask.end()));

  return result;
}

template<typename Container>
void PrintVi(const Container& vi) {
  for (auto& i : vi) {
    cout << i << " ";
  }
  cout << "\n";
}

void PrintVVi(const vector<vector<int>>& vvi) {
  for (auto& vi : vvi) {
    PrintVi(vi);
  }
}

TEST_CASE("Combinatorics: subsets of size k", "[Combinatorics]") {
  cout << "{1,2,3,4} - 2\n";
  PrintVVi( SubsetsOfK({1,2,3,4}, 2) );
  cout << "{1,2,3,4} - 1\n";
  PrintVVi( SubsetsOfK({1,2,3,4}, 1) );
  cout << "{1,2,3,4} - 4\n";
  PrintVVi( SubsetsOfK({1,2,3,4}, 4) );
}


void RandomizeArray(vector<int>& array) {
  default_random_engine gen(random_device{}());
  for (int i = array.size()-1; i >= 1; --i) {
    int j = uniform_int_distribution<int>{0, i}(gen);
    swap(array[i], array[j]);
  }
}

TEST_CASE("Combinatorics: randomize array", "[Combinatorics]") {
  vector<int> array = {1,2,3,4,5,6,7,8,9,10};
  RandomizeArray(array);
  PrintVi(array);
  RandomizeArray(array);
  PrintVi(array);
  RandomizeArray(array);
  PrintVi(array);
  RandomizeArray(array);
  PrintVi(array);
}

vector<int> RandomKSample(const vector<int>& stream, int k) {
  default_random_engine gen(random_device{}());
  vector<int> sample;
  for (int i = 0; i < k; ++i) {
    sample.push_back(stream[i]);
  }

  for (int i = k; i < stream.size(); ++i) {
    int r = uniform_int_distribution<int>{0, i}(gen);
    if (r < k) {
      sample[r] = stream[i];
    }
  }

  
  return sample;
}

TEST_CASE("Combinatorics: random k sample", "[Combinatorics]") {
  vector<int> array = {1,2,3,4,5,6,7,8,9,10};
  PrintVi(RandomKSample(array, 2));
  PrintVi(RandomKSample(array, 2));
  PrintVi(RandomKSample(array, 2));

  PrintVi(RandomKSample(array, 1));
  PrintVi(RandomKSample(array, 1));
  PrintVi(RandomKSample(array, 1));
}

template<typename It>
bool NextPermutation(It begin, It end) {
  if (begin == end) {
    // empty input is already sorted
    return false;
  }

  It prev = end;
  It last = end;
  --last;
  while (last != begin && *last >= *prev) {
    --last; --prev;
  }
  if (*last >= *prev) {
    // already reverse sorted
    return false;
  }
  // *last < *prev
  It to_swap = last;

  while (prev != end && *prev > *to_swap) {
    ++last; ++prev;
  }

  swap(*to_swap, *last);

  // reverse [to_swap+1, end)
  ++to_swap;
  while (end-to_swap > 1) {
    --end;
    swap(*end, *to_swap);
    ++to_swap;
  }

  return true;
}

void checkNP(const string& to_test) {
  auto s = to_test;
  vector<string> tested;
  do {
    tested.push_back(s);
  } while (NextPermutation(s.begin(), s.end()));

  s = to_test;
  vector<string> expected;
  do {
    expected.push_back(s);
  } while (next_permutation(s.begin(), s.end()));

  PrintVi(tested);
  PrintVi(expected);
  REQUIRE( tested == expected );
}

TEST_CASE("Combinatorics: next permutation", "[Combinatorics]") {
  checkNP("123");
  checkNP("213");
  checkNP("1");
  checkNP("");
  checkNP("00011");
  checkNP("1122344");
}


long long NChooseKHelper(long long n, long long k, vector<vector<long long>>& memo) {
  if (n == 0 || n == 1) return 1; 
  if (k == 0 || k == n) return 1;
  if (memo[n][k] != -1) return memo[n][k];
  long long result = NChooseKHelper(n-1, k-1, memo) + NChooseKHelper(n-1, k, memo);
  return memo[n][k] = result;
}

long long NChooseK(long long n, long long k) {
  vector<vector<long long>> memo(n+1, vector<long long>(k+1, -1));
  return NChooseKHelper(n, k, memo);
}


TEST_CASE("Combinatorics: n choose k", "[Combinatorics]") {
  REQUIRE( NChooseK(0, 0) == 1 );
  
  REQUIRE( NChooseK(1, 0) == 1 );
  REQUIRE( NChooseK(1, 1) == 1 );
  
  REQUIRE( NChooseK(2, 1) == 2 );
  
  REQUIRE( NChooseK(3, 0) == 1 );
  REQUIRE( NChooseK(3, 1) == 3 );
  REQUIRE( NChooseK(3, 2) == 3 );
  REQUIRE( NChooseK(3, 3) == 1 );
  
  REQUIRE( NChooseK(5, 3) == 10 );

  REQUIRE( NChooseK(64, 31) == 1777090076065542336 );
  REQUIRE( NChooseK(54, 33) == 520341450264090 );
}


