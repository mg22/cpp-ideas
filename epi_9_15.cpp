#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

struct Node {
  Node() : Node(nullptr, nullptr, 'X') {}
  Node(char _val) : Node(nullptr, nullptr, _val) {}
  Node(unique_ptr<Node> _left, unique_ptr<Node> _right, char _val) :
    left(move(_left)), right(move(_right)), val(_val) {}
  unique_ptr<Node> left;
  unique_ptr<Node> right;
  char val;
};


void ExteriorHelper(const unique_ptr<Node>& current, vector<const unique_ptr<Node>*>& output, bool left_edge, bool right_edge) {
  if (!current->left && !current->right) {
    // leaf
    output.push_back(&current);
    return;
  }

  if (left_edge) {
    output.push_back(&current);
  }
  
  if (current->left) {
    ExteriorHelper(current->left, output, left_edge, right_edge && !current->right);
  }

  if (current->right) {
    ExteriorHelper(current->right, output, left_edge && !current->left, right_edge);
  }

  if (right_edge) {
    output.push_back(&current);
  }
}

vector<const unique_ptr<Node>*> Exterior(const unique_ptr<Node>& root) {
  vector<const unique_ptr<Node>*> output;
  output.push_back(&root);
  if (root->left) {
    ExteriorHelper(root->left, output, true, false);
  }
  if (root->right) {
    ExteriorHelper(root->right, output, false, true);
  }
  return output;
}

TEST_CASE("EPI_9_15: Basic tests", "[EPI_9_15]") {
  // tree
  auto root = (
    make_unique<Node>(
      make_unique<Node>(
        make_unique<Node>(
          make_unique<Node>('D'),
          make_unique<Node>('E'),
          'C'
        ),
        make_unique<Node>(
          nullptr,
          make_unique<Node>(
            make_unique<Node>('H'),
            nullptr,
            'G'),
          'F'
        ),
        'B'
      ),
      make_unique<Node>(
        make_unique<Node>(
          nullptr,
          make_unique<Node>(
            make_unique<Node>(
              nullptr,
              make_unique<Node>('M'),
              'L'),
            make_unique<Node>('N'),
            'K'),
          'J'),
        make_unique<Node>(
          nullptr,
          make_unique<Node>('P'),
          'O'),
        'I'),
      'A')
  );


  auto exterior = Exterior(root);
  vector<char> expected = {'A', 'B', 'C', 'D', 'E', 'H', 'M', 'N', 'P', 'O', 'I'};
  REQUIRE( exterior.size() == expected.size() );
  for (size_t i = 0; i < exterior.size(); ++i) {
    REQUIRE( exterior[i]->get()->val == expected[i] );
  }
  // for (auto* x : exterior) {
  //   cout << (*(*x)).val << "\n";
  // }
}