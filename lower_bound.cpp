#include <catch2/catch.hpp>
#include <cassert>
#include <vector>
#include <random>
#include <algorithm>
#include <limits>


int lower_bound1(const std::vector<int>& v, int start, int end, int val) {
    if (start == end) {
        return start;
    }

    int result = end;
    --end;

    while (start <= end) {
        int mid = start + (end-start)/2;
        if (v[mid] >= val) {
            result = mid;
            end = mid-1;
        } else {
            start = mid+1;
        }
    }

    return result;
}


TEST_CASE("LowerBound1: Sanity check", "[LowerBound]") {
    //                    0  1  2  3  4  5   6   7   8   9   10  11
    std::vector<int> v = {2, 2, 3, 7, 8, 42, 47, 47, 47, 48, 55, 100};
    auto search = [&](int x) {
        return lower_bound1(v, 0, v.size(), x);
    };

    REQUIRE( search(8) == 4 );
    REQUIRE( search(42) == 5 );
    REQUIRE( search(47) == 6 );
    REQUIRE( search(48) == 9 );
    REQUIRE( search(200) == 12 );
    REQUIRE( search(1) == 0 );
}

TEST_CASE("LowerBound1: Consistent with std::lower_bound", "[LowerBound]") {

    constexpr int TEST_CASES = 200;
    constexpr int TEST_CASE_SIZE = 100000;
    constexpr int TEST_CASE_SEARCHES = 1000;
    constexpr int MIN_ELEMENT = 100;
    constexpr int MAX_ELEMENT = 1000;

    std::default_random_engine re(std::random_device{}());
    std::uniform_int_distribution<int> el_dist(MIN_ELEMENT, MAX_ELEMENT);

    for (int t = 0; t < TEST_CASES; ++t) {
        std::vector<int> to_test; to_test.reserve(TEST_CASE_SIZE);
        for (int i = 0; i < TEST_CASE_SIZE; ++i) {
            to_test.push_back(el_dist(re));
        }
        std::sort(to_test.begin(), to_test.end());

        for (int s = 0; s < TEST_CASE_SEARCHES; ++s) {
            int el = el_dist(re);
            auto it = std::lower_bound(to_test.begin(), to_test.end(), el);
            int i = lower_bound1(to_test, 0, to_test.size(), el);
            REQUIRE( i == (it-to_test.begin()) ); 
        }
    }
}
