#include <catch2/catch.hpp>
#include <bits/stdc++.h>

using namespace std;

int Pivotize(vector<int>& array, int left, int right, int pivot_index) {
  // assuming left <= pivot_index <= right
  // reorganize [left,right] part of array, so it's [< pivot ... pivot ... > pivot]
  int pivot_val = array[pivot_index];
  // move pivot aside
  swap(array[right], array[pivot_index]);
  int next_smaller = left;
  for (int i = left; i < right; ++i) {
    if (array[i] < pivot_val) {
      swap(array[i], array[next_smaller]);
      ++next_smaller;
    }
  }
  swap(array[next_smaller], array[right]);
  return next_smaller;
}

int KthElement(vector<int>& array, int k) {
  int left = 0;
  int right = array.size()-1;

  default_random_engine gen(random_device{}());

  while (left <= right) {
    int pivot_index = uniform_int_distribution<int>(left, right)(gen);
    int new_pivot_index = Pivotize(array, left, right, pivot_index);
    if (new_pivot_index == k) {
      return array[k];
    } else if (new_pivot_index < k) {
      left = new_pivot_index + 1;
    } else /* new_pivot_index > k */ {
      right = new_pivot_index - 1;
    }
  }
}

TEST_CASE("EPI_11_8: Basic tests", "[EPI_11_8]") {
  // -1 2 3 4 5 6 8
  vector<int> v = {3, -1, 2, 6, 4, 5, 8};

  REQUIRE( KthElement(v, 0) == -1 );
  REQUIRE( KthElement(v, 1) == 2 );
  REQUIRE( KthElement(v, 2) == 3 );
  REQUIRE( KthElement(v, 3) == 4 );
  REQUIRE( KthElement(v, 4) == 5 );
  REQUIRE( KthElement(v, 5) == 6 );
  REQUIRE( KthElement(v, 6) == 8 );

  vector<int> v2 = { 1 };

  REQUIRE( KthElement(v2, 0) == 1 );
}